<?php

namespace Laraden\Facades;

class Flash extends \Illuminate\Support\Facades\Facade
{
	protected static function getFacadeAccessor()
	{
		return "flash";
	}
}