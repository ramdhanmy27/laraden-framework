<?php

namespace Laraden\Providers;

use Collective\Html\HtmlServiceProvider as CollectiveServiceProvider;
use Laraden\Support\View\FormBuilder;
use Laraden\Support\View\HtmlBuilder;

class HtmlServiceProvider extends CollectiveServiceProvider
{
    /**
     * Register the form builder instance.
     *
     * @return void
     */
    protected function registerFormBuilder()
    {
        $this->app->singleton('form', function ($app) {
            $form = new FormBuilder(
                $app['html'], 
                $app['url'], 
                $app['view'], 
                $app['session.store']->token(), 
                $app['request']
            );

            return $form->setSessionStore($app['session.store']);
        });
    }

    /**
     * Register the HTML builder instance.
     *
     * @return void
     */
    protected function registerHtmlBuilder()
    {
        $this->app->singleton('html', function ($app) {
            return new HtmlBuilder($app['url'], $app['view']);
        });
    }
}
