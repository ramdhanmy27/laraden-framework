<?php

namespace Laraden\Providers;

use Laraden\Support\Data\JsonStorage;
use Closure;
use Event;
use Illuminate\Support\ServiceProvider;
use Laraden\Http\Modular;
use Laraden\Providers\HtmlServiceProvider;
use Laraden\Support\Auth\Permission;
use Laraden\Support\Helpers\Messages;
use Laraden\Support\Menu\MenuFactory;
use Laraden\Support\View\Theme;
use Menu;
use Route;
use Yajra\Datatables\DatatablesServiceProvider;

class LaradenServiceProvider extends ServiceProvider
{
    protected $providers = [
        DatatablesServiceProvider::class,
        HtmlServiceProvider::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!defined("LARADEN_ROOT")) {
            define("LARADEN_ROOT", realpath(__DIR__."/../.."));
        }

        $this->mergeConfigFrom(LARADEN_ROOT.'/config/config.php', 'laraden');
        $this->mergeConfigFrom(LARADEN_ROOT.'/config/entrust.php', 'entrust');

        // Configure Class Aliases
        $this->configureAliases();

        // Register Service Containers
        $this->registerServices();

        // Register Providers
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() 
    {
        $this->loadMigrationsFrom(LARADEN_ROOT."/database/migrations");
        
        // Views
        $theme = config("laraden.theme");
        $this->loadViews($theme);

        // Publishes
        $this->registerPublishes(compact("theme"));

        // Load console commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Laraden\Commands\MenuCommand::class,
            ]);
        }

        // Enable Modular
        if (config("laraden.modular.enable")) {
            if (config("laraden.domain", false)) {
                Route::domain(config("laraden.domain"))->group(function() {
                    app("modular")->init();
                });
            }
            else app("modular")->init();
        }
    }

    protected function configureAliases()
    {
        $data = [
            'Datatables' => \Yajra\Datatables\Facades\Datatables::class,
            'Form' => \Collective\Html\FormFacade::class,
            'Html' => \Collective\Html\HtmlFacade::class,
            'Menu' => \Laraden\Facades\Menu::class,
            'Flash' => \Laraden\Facades\Flash::class,
            'Notif' => \Laraden\Facades\Notif::class,
        ];

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        foreach ($data as $alias => $class) {
            $loader->alias($alias, $class);
        }
    }

    protected function registerServices() 
    {
        $this->app->singleton("menu", function() {
            return new MenuFactory;
        });
        
        $this->app->singleton("permission", function() {
            return new Permission;
        });
            
        $this->app->singleton("notif", function() {
            return new Messages("notif");
        });
        
        $this->app->singleton("flash", function() {
            return new Messages("flash");
        });
        
        $this->app->singleton("modular", function() {
            return new Modular(config("laraden.modular.path"));
        });
        
        $this->app->singleton("storage.config", function() {
            return new JsonStorage(config("laraden.storage.config.path"));
        });
    }

    protected function loadViews($theme = null)
    {
        \View::addLocation(LARADEN_ROOT."/resources/views");
        
        if ($theme) {
            \View::addLocation(LARADEN_ROOT."/resources/themes/$theme/views");
            $this->loadViewsFrom(LARADEN_ROOT."/resources/themes/$theme/views/errors", "errors");
        }
    }

    protected function registerPublishes(array $args)
    {
        $this->publishes([
            // config
            LARADEN_ROOT."/config/config.php" => config_path("laraden.php"),

            // Model & Module
            LARADEN_ROOT."/publish/app" => base_path("app"),
            LARADEN_ROOT."/publish/.gitkeep" => base_path("public/app/.gitkeep"),

            // translation
            resource_path("lang/en") => resource_path("lang/id"),
            LARADEN_ROOT."/resources/lang/id" => resource_path("lang/id"),

            // gulpfile
            LARADEN_ROOT."/gulpfile.js" => base_path("gulpfile.js"),

            // migrations
            // LARADEN_ROOT."/migrations" => base_path("database/migrations"),
        ], "laraden");

        // Views & Assets
        $publish = [
            LARADEN_ROOT."/resources/assets" => resource_path("assets"),
            LARADEN_ROOT."/resources/views" => resource_path("views"),
            // LARADEN_ROOT."/resources/themes/$theme/public" => public_path(),
        ];

        if (isset($args["theme"]) && $args["theme"]) {
            $publish = array_merge($publish, [
                LARADEN_ROOT."/resources/themes/$args[theme]/assets" => resource_path("assets"),
                LARADEN_ROOT."/resources/themes/$args[theme]/views" => resource_path("views"),
            ]);
        }

        $this->publishes($publish, "theme");

        // Documentation
        $this->publishes([
            LARADEN_ROOT."/docs" => base_path("docs"),
        ], "docs");

        // Modules
        $this->publishes([
            LARADEN_ROOT."/modules" => app_path("Modules"),
        ], "modules");
    }
}
