<?php

namespace Laraden\DB;

use Closure;
use Laraden\Exceptions\ValidatorException;
use LaravelArdent\Ardent\Ardent;
use Validator;

class Model extends Ardent 
{
    public function __construct(array $attributes = [])
    {
        if (method_exists($this, "defaultValue")) {
            $this->attributes = array_merge($this->attributes, $this->defaultValue());
        }

        parent::__construct($attributes);
    }

    public static function instance()
    {
        return new static;
    }

    public function hasKey() 
    {
        return isset($this->attributes[$this->primaryKey]);
    }

    public function label($name, $default = null) 
    {
        if ($default===null) {
            $default = title_case($name);
        }

        if (method_exists($this, "attributeLabels")) {
            return array_get($this->attributeLabels(), $name, $default);
        }
        
        return $default;
    }

    public function setAttribute($key, $value)
    {
        $rules = method_exists($this, "rules") ? $this->rules() : [];

        // change empty string to null on several validation rules
        if (is_string($value) && trim($value)=="" && isset($rules[$key])) {
            foreach (["date", "integer", "numeric", "date_format"] as $rule) {
                if (strpos($rules[$key], $rule) !== false) {
                    $value = null;
                    break;
                }
            }
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * @inheritDoc
     */
    public static function createOrFail(array $data = []) 
    {
        $model = new static($data);

        $args = func_get_args();
        array_shift($args);
        call_user_func_array([$model, "saveOrFail"], $args);

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function updateOrFail(array $data = [], array $opt = []) 
    {
        if (!$this->exists) {
            return false;
        }

        return $this->fill($data)->saveOrFail([], [], $opt);
    }

    /**
     * Save the model to the database using transaction.
     *
     * @param  array  $options
     * @return bool
     *
     * @throws \Throwable
     */
    public function saveOrFail(
        array $rules = [], 
        array $msg = [], 
        array $opt = [], 
        Closure $before = null, 
        Closure $after = null) 
    {
        $success = $this->save($rules, $msg, $opt, $before, $after);

        if (!$success) {
            throw new ValidatorException($this->validator);
        }

        return $success;
    }

    public function afterValidate()
    {
        $rules = method_exists($this, "rules") ? $this->rules() : [];

        foreach ($rules as $field => $rule) {
            if (!isset($this->{$field})) {
                continue;
            }

            // change empty string to null
            if (strpos($rule, "integer") !== false && is_scalar($this->{$field})) {
                $this->{$field} = trim($this->{$field})=="" ? null : $this->{$field};
            }
        }
    }

    public function scopeRange($query, $field, array $value) 
    {
        if ($date = current($value)) {
            $query->where($field, ">=", $date);
        }

        if ($date = next($value)) {
            $query->where($field, "<=", $date);
        }
    }
}
