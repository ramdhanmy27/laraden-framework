<?php

namespace Laraden\Exceptions;

use Illuminate\Support\MessageBag;
use Illuminate\Validation\Validator;

class ValidatorException extends \Exception 
{
	private $messages = [];
	private $validator;

	public function __construct($messages, $code = 0, Exception $previous = null) 
	{
		if ($messages instanceof MessageBag)
			$this->messages = $messages;
		elseif ($messages instanceof Validator) {
			$this->validator = $messages;
			$this->messages = $messages->errors();
		}
		else $this->messages = new MessageBag(!is_array($messages) ? [$messages] : $messages);

		parent::__construct($this->messages->first(), $code, $previous);
	}

	public function getMessages() 
	{
		return $this->messages;
	}

	public function getValidator() 
	{
		return $this->validator;
	}
}