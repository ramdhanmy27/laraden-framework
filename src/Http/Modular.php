<?php

namespace Laraden\Http;

use Event;
use Illuminate\Support\Facades\File;
use Laraden\Facades\Menu;
use Request;
use Route;
use StdClass;
use View;

class Modular
{
    private $paths = [];

    protected $registered = [];

    public $module;

    public $current;

    public function __construct(array $paths)
    {
        $this->paths = $paths;
    }

    public function init()
    {
        // separate menu per module
        Menu::driver("collection")->enable = !config("laraden.modular.separation.menu");

        // Register modules
        foreach ($this->paths as $path => $namespace) {
            $this->register($path, rm_slash($namespace));
        }
    }

	public function register($path, $namespace)
	{
        $realpath = base_path($path);

        $module = new StdClass();
        $this->module = $module;

        // register modules route
        foreach (glob("$realpath/*", GLOB_ONLYDIR) as $path) {
            $module->path = $path;
            $module->name = basename($path);
            // $module->name = rm_slash(str_replace($realpath, '', $module->path)); // recurse name
            $module->namespace = "\\$namespace\\{$module->name}";
            $module->id = hyphen_case($module->name);

            // Register Module
            $this->registerModule(clone $module);
        }

        $this->paths[$path] = $namespace;
        $this->module = null;
	}

    public function getRegisteredModule($module_id = null)
    {
        if ($module_id == null) {
            return $this->registered;
        }
        else if (isset($this->registered[$module_id])) {
            return $this->registered[$module_id];
        }
        else return null;
    }

    protected function registerModule($module)
    {
        $this->loadConfig($module);

        $this->loadProviders($module);

        $this->loadMigrations($module);

        $this->loadRoutes($module);

        $this->registered[$module->id] = $module;
        Event::fire("module.register", $module);
    }

    protected function loadProviders($module)
    {
        $dir = $module->path."/Providers";
        $providers = [];

        // Trigger Providers
        if (file_exists($dir)) {
            foreach (glob("$dir/*ServiceProvider.php") as $file) {
                include_once $file;

                $class_name = $module->namespace."\\Providers\\".pathinfo($file, PATHINFO_FILENAME);

                if (!class_exists($class_name)) {
                    continue;
                }

                app()->register($class_name);
            }
        }
    }

    protected function loadConfig($module)
    {
        $config = [];

        try {
            // load config file
            foreach (glob($module->path."/Config/*.php") as $file) {
                $pathinfo = pathinfo($file);
                $config[$pathinfo["filename"]] = include $file;
            }
        }
        catch (\Exception $e) {}

        // merge config
        if (count($config) > 0) {
            $config = array_merge(app("config")->get($module->id, []), $config);
            app("config")->set($module->id, $config);
        }
    }

    protected function loadMigrations($module)
    {
        $dir = $module->path."/Migrations";

        if (file_exists($dir)) {
            app("migrator")->path($dir);
        }
    }

    protected function loadRoutes($module)
    {
        $route_file = "$module->path/routes.php";
        $ns_controller = "$module->namespace\\Controllers";

        if (file_exists($route_file)) {
            $prefix = config("laraden.modular.map.$module->name", $module->id);

            // Register routes
            Route::group([
                "namespace" => $ns_controller, 
                "prefix" => $prefix, 
                "middleware" => ["web"],
            ], function() use ($route_file) {
                require $route_file;
            });

            // Module matched
            if (strpos(Request::path()."/", $prefix."/") === 0 
                || (Request::path()=="/" && $prefix==null)) {
                Menu::driver("collection")->enable = true;
                $this->current = $module;
                $this->loadModule($module);

                // Trigger event on Load
                Event::fire("module.load", $module);
            }
        }
    }

    protected function loadModule($module)
    {
        if (defined("MODULE_ID")) {
            return;
        }

        define("MODULE_ID", $module->id);
        define("MODULE_NAME", $module->name);
        define("MODULE_PATH", $module->path);

        // Load Translations
        app("translator")->addNamespace($module->id, $module->path."/Resources/lang");

        // Register view to view finder
        View::addLocation($module->path."/Resources/views");
        View::addNamespace("app", $module->path."/Resources/views");

        // load helpers
        $helper_path = "$module->path/Helpers";

        if (file_exists($helper_path)) {
            foreach (glob($helper_path."/*.php") as $filepath) {
                require_once $filepath;
            }
        }

        // copy assets to public
        if (config("laraden.modular.enable") && config("laraden.modular.assets.autoload")) {
            File::copyDirectory(
                "$module->path/Resources/assets", 
                base_path("public/app/$module->id")
            );
        }
    }
}
