<?php

namespace Laraden\Http\Middleware;

use Closure;
use Illuminate\View\View;

class ViewResolver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $res = $next($request);

        // render partial (content section only)
        // request is ajax and using view blade template
        if ($request->ajax() && isset($res->original) && $res->original instanceof View) {
            $sections = $res->original->renderSections();

            if (isset($sections["content-ajax"])) {
                $res->setContent($sections['content-ajax']);
            }
            else if (isset($sections["content"])) {
                $res->setContent($sections['content']);
            }
        }

        return $res;
    }
}
