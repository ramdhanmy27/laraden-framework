<?php

namespace Laraden\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthorizePermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $credentials = array_slice(func_get_args(), 2);

        can_or_fail($credentials);

        return $next($request);
    }
}
