<?php

namespace Laraden\Commands;

use DB;
use Illuminate\Console\Command;
use Laraden\Http\Modular;
use Laraden\Support\Menu\Model;
use Menu;
use Route;

class MenuCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "menu:migrate {--refresh}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrate menu collection into database.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        echo "Migrating Menu Router..\r\n\r\n";

        DB::transaction(function() {
            if ($this->option("refresh")) {
                Model::query()->delete();
            }

            $this->migrate(Menu::driver("collection")->getTree());
        });

        echo "\r\nDone!\r\n";
    }

    protected function migrate($tree, $parent_id = null, $level = 1)
    {
        $childless = [];
        $order = 0;

        foreach ($tree as $item) {
            $param = array_filter($item->getAttr());

            $data = [
                "parent" => $parent_id, 
                "label" => $item->label, 
                "url" => $item->url, 
                "enable" => true, 
                "order" => $order++,
                "module" => $item->getAttr("module"),
                "param" => count($param) > 0 ? json_encode($param) : null,
            ];

            echo str_fill("     ", $level<=2 ? 0 : $level-2)
                .($level==1 ? "" : "  |--")
                ." $item->label\r\n";

            if ($item->hasChildren()) {
                $parent = Model::createOrFail($data);
                $this->migrate($item->child, $parent->id, $level+1);
            }
            else $childless[] = $data;
        }

        Model::insert($childless);
    }
}
