<?php

namespace Laraden\Support\View;

use Collective\Html\HtmlBuilder as CollectiveHtmlBuilder;

class HtmlBuilder extends CollectiveHtmlBuilder 
{
    public function icon($class) 
    {
        return "<i class='$class'></i>";
    }
}