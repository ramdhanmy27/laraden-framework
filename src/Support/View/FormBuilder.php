<?php

namespace Laraden\Support\View;

use App\Models\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

class FormBuilder extends \Collective\Html\FormBuilder 
{
    protected $form_id;

    protected $rules = [];

    protected $labels = [];

    public function rules($rules) 
    {
        if (!is_array($rules)) {
            return;
        }

        $this->rules = array_merge($this->rules, $rules);
    }

    public function labels($labels) 
    {
        if (!is_array($labels)) {
            return;
        }

        $this->labels = array_merge($this->labels, $labels);
    }

    public function registerModel($model) 
    {
        if (method_exists($model, "attributeLabels")) {
            $this->labels($model->attributeLabels());
        }

        if (method_exists($model, "rules")) {
            $this->rules($model->rules());
        }
    }

    protected function option($display, $value, $selected, array $attributes = [])
    {
        return parent::option(trans($display), $value, $selected, $attributes);
    }

    public function group($type, $name, $attributes = []) 
    {
        // Form Attributes
        if (!isset($attributes["attr"])) {
            $attributes["attr"] = [];
        }
        
        // Form ID
        if (!isset($attributes["attr"]["id"])) {
            $attributes["attr"]["id"] = $name.$this->form_id;
        }

        // Form Label
        if (!isset($attributes["label"])) {
            if (method_exists($this->model, "label")) {
                $attributes["label"] = $this->model->label($name);
            }
            else $attributes["label"] = title_case($name);
        }

        // Form Input
        $input_method = strtolower(camel_case($type));
        $value = isset($attributes["value"]) ? $attributes["value"] : null;

        switch ($input_method) {
            case "label":
                $attributes["attr"]["class"] = "text-left control-label";
                $attributes["attr"]["style"] = "font-weight: bold";
            case "img":
            case "date":
            case "daterange":
            case "time":
            case "datetime":
            case "textarea":
                $input = call_user_func_array([$this, $input_method], [
                    $name, $value, $attributes["attr"]
                ]);
                break;

            case "toggleSwitch":
            case "radio":
            case "checkbox":
                $input = call_user_func_array([$this, $input_method], [
                    $name, 
                    $value, 
                    isset($attributes["checked"]) ? $attributes["checked"] : null, 
                    $attributes["attr"]
                ]);
                break;

            case "checkboxes":
            case "radios":
            case "select":
                $input = call_user_func_array([$this, $input_method], [
                    $name, 
                    isset($attributes["options"]) ? $attributes["options"] : [], 
                    $value===null ? [] : $value, 
                    $attributes["attr"]
                ]);
                break;
            
            default:
                $input = $this->input($type, $name, $value, $attributes["attr"]);
                break;
        }

        return "<div class='form-group'>".
                $this->label($attributes["attr"]["id"], $attributes["label"])
                ."<div class='col-md-7'>$input</div>"
            ."</div>";
    }

    public function getFieldRule($name) 
    {
        $name = preg_replace("/\[\w*\]/", "", $name);

        if (empty($this->rules) || !isset($this->rules[$name])) {
            return null;
        }

        return $this->rules[$name];
    }

    protected function mergeOptions($name = null, $options = []) 
    {
        $defaultOpt = ["class" => "form-control"];

        if ($rules = $this->getFieldRule($name)) {
            $defaultOpt["rules"] = $rules;
        }

        return $options + $defaultOpt;
    }

    /** Overrides */

    public function model($model, array $options = []) 
    {
        if (is_string($model)) {
            $model = new $model;
        }

        $this->registerModel($model);

        return parent::model($model, $options);
    }

    public function open(array $options = []) 
    {
        $this->form_id = uniqid();

        return parent::open($options + ["class" => "form-horizontal"]);
    }

    public function label($name, $value = null, $options = [], $escape = true) 
    {
        return parent::label(
            $name, 
            $value===null ? $this->model->getAttribute($name) : $value, 
            $options + ["class" => "col-md-3 control-label"], 
            $escape
        );
    }

    public function close() 
    {
        $this->rules = [];
        $this->labels = [];
        $this->form_id = null;

        return parent::close();
    }

    /** Inputs */

    public function input($type, $name, $value = null, $options = []) 
    {
        return parent::input($type, $name, $value, $this->mergeOptions($name, $options));
    }

    public function img($name, $preview = null, $options = [])
    {
        if ($preview === null) {
            $preview = $this->getModelValueAttribute($name);
        }

        if (!empty($preview)) {
            $options["preview"] = url($preview);
        }

        return $this->file($name, $options);
    }

    public function date($name, $value = null, $options = []) 
    {
        return $this->text(
            $name, 
            $value, 
            $this->mergeOptions($name, $options + ["datepicker" => ""])
        );
    }

    public function daterange($name, $value = null, $options = []) 
    {
        return $this->text(
            $name, 
            is_array($value) ? implode(" - ", $value) : $value, 
            $this->mergeOptions($name, $options + ["daterangepicker" => ""])
        );
    }

    public function time($name, $value = null, $options = []) 
    {
        $options["timepicker"] = "";

        return $this->text($name, $value, $this->mergeOptions($name, $options));
    }

    public function checkboxes($name, $options = [], $checked = []) 
    {
        if ($checked instanceof Collection) {
            $checked = $checked->toArray();
        }

        $html = [];

        foreach ($options as $value => $label) {
            $html[] = $this->checkbox(
                $name, 
                $value, 
                count($checked)==0 ? null : in_array($value, $checked), 
                ["label" => $label]
            );
        }

        return implode("", $html);
    }

    public function datetime($name, $value = null, $options = []) 
    {
        return $this->text(
            $name, 
            $value, 
            $this->mergeOptions($name, $options + ["datetimepicker" => ""])
        );
    }

    public function select(
        $name, 
        $list = [], 
        $selected = null, 
        array $options = [], 
        array $opt_attr = []
    ) {
        if (isset($this->model)) {
            $selected = ($this->model->{$name});
        }

        // if ($selected == null)
        //     $selected = $this->missingOldAndModel($name);

        if (is_array($selected)) {
            $selected = implode(",", $selected);
        }

        $options = $this->mergeOptions($name, $options + [
            "select2" => "", 
            "placeholder" => "Select..", 
            "value" => $selected,
            "class" => null,
        ]);

        return parent::select($name, $list, $selected, $options, $opt_attr);
    }

    public function checkbox($name, $value = 1, $checked = null, $options = []) 
    {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();
        
        $options = $this->mergeOptions($name, $options + [
            "id" => $id, 
            "class" => "colored-success",
        ]);

        $form = parent::checkbox($name, $value, $checked, $options);

        if (View::exists("form.checkbox")) {
            return view("form.checkbox", compact("form", "options"))->render();
        }
        else {
            return "<div class='checkbox ".@$options['class']."'>"
                    ."<label>"
                        .$form."<span class='text'>".@$options['label']."</span>"
                    ."</label>
                </div>";
        }
    }

    public function toggleSwitch($name, $value = 1, $checked = null, $options = []) 
    {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();
        $options = $this->mergeOptions($name, $options + [
            "id" => $id,
            "class" => "checkbox-slider colored-darkorange",
        ]);
        $form = parent::checkbox($name, $value, $checked, $options);

        if (View::exists("form.toggle-switch")) {
            return view("form.toggle-switch", compact("form", "options"))->render();
        }
        else {
            return "<label class='mt-sm'>"
                    .$form." <span class='text'></span>"
                ."</label>";
        }
    }

    public function radio($name, $value = null, $checked = null, $options = []) 
    {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();
        $options = $this->mergeOptions($name, $options + [
            "id" => $id, 
            "class" => "colored-blue",
        ]);
        $form = parent::radio($name, $value, $checked, $options);

        if (View::exists("form.radio")) {
            return view("form.radio", compact("form", "options"))->render();
        }
        else {
            return "<div class='radio ".@$options['class']."'>"
                    ."<label>"
                        .$form."<span class='text'>".@$options['label']."</span>"
                    ."</label>
                </div>";
        }
    }

    public function radios($name, $options = [], $checked = null) 
    {
        $html = [];

        foreach ($options as $value => $label) {
            $html[] = $this->radio(
                $name, 
                $value, 
                $value==$checked, 
                ["label" => $label]
            );
        }

        return implode("", $html);
    }

    public function textarea($name, $value = null, $options = ["rows" => 5]) 
    {
        return parent::textarea($name, $value, $this->mergeOptions($name, $options));
    }

    /** Buttons */

    public function submit($value = null, $options = []) 
    {
        return parent::submit($value, $options + ["class" => "btn btn-primary"]);
    }

    public function button($value = null, $options = []) 
    {
        return parent::button($value, $options + ["class" => "btn btn-default"]);
    }
}