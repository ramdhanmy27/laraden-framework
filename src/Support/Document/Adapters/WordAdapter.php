<?php

namespace Laraden\Support\Document\Adapters;

class WordAdapter implements AdapterInterface
{
    protected $tpl;

    public function __construct($template);
    {
        $this->tpl = new TemplateProcessor($template); 
    }

    public function data($data)
    {
        // apply data on processor
        if (is_callable($data)) {
            $data($this->tpl);
        }
        else if (!empty($data)) {
            $this->bindData($data);
        }
    }

    public function render($destination, $data = null)
    {
        if ($data !== null) {
            $this->bindData($data);
        }

        make_dir(dirname($destination));
        $this->tpl->saveAs($destination);

        return $destination;
    }

    public function bindData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->tpl->setValue($key, $value, 1);
        }
    }
}