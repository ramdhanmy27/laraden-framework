<?php

namespace Laraden\Support\Document\Adapters;

interface AdapterInterface
{
    public function __construct($template);

    public function data($data);

    public function render($destination, $data = null);
}