<?php

namespace Laraden\Support\Document\Adapters;

class BladeAdapter implements AdapterInterface
{
    protected $template;

    protected $data = [];

    public function __construct($template)
    {
        $this->template = $template;
    }

    public function data($data)
    {
        if (is_array($data)) {
            $this->data = array_merge($this->data, $data);
        }
    }

    public function render($destination, $data = null)
    {
        if ($data !== null) {
            $this->data($data);
        }

        $view = view()->file($this->template, $this->data);
        file_put_contents($destination, $view->render());

        return $destination;
    }
}