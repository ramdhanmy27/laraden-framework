<?php

namespace Laraden\Support\Document\Adapters;

use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor as Origin;

Settings::setOutputEscapingEnabled(true);

class TemplateProcessor extends Origin
{
	public function cloneBlock($blockname, $clones = 1, $replace = true)
    {
        $xmlBlock = null;
        preg_match(
            '/(<\?xml.*)(<w:p( [^>]*)?>([\s]*<.*>)?\${' . $blockname . '}(<.*?>[\s]*)?<\/w:p>)(.*)(<w:p( [^>]*)?>([\s]*<.*>)?\${\/' . $blockname . '}(<.*?>[\s]*)?<\/w:p>)/is',
            $this->tempDocumentMainPart,
            $matches
        );

        if (isset($matches[6])) {
            $xmlBlock = $matches[6];
            $cloned = array();
            for ($i = 1; $i <= $clones; $i++) {
                $cloned[] = $xmlBlock;
            }

            if ($replace) {
                $this->tempDocumentMainPart = str_replace(
                    $matches[2] . $matches[6] . $matches[7],
                    implode('', $cloned),
                    $this->tempDocumentMainPart
                );
            }
        }

        return $xmlBlock;
    }
}