<?php

namespace Laraden\Support\Document;

use Closure;
use Laraden\Support\Document\Adapters\BladeAdapter;
use Laraden\Support\Document\Adapters\WordAdapter;

class Convert
{
    protected $tpl, $tpl_pathinfo;

    protected $adapter;

    protected $data;

    public function __construct($file)
    {
        set_time_limit(0);

        $this->tpl = $file;
        $this->tpl_pathinfo = pathinfo($this->tpl);

        $this->adapter = $this->resolveAdapter($this->tpl);
    }

    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->adapter, $method], $arguments);
    }

    protected static function unoconv()
    {
        return "python \"".LARADEN_ROOT."/bin/unoconv\"";
    }

    protected function resolveAdapter($filepath)
    {
        $arr = explode(".", basename($filepath));
        $offset = count($arr)-2;
        $extension = implode(".", array_slice($arr, $offset<=0 ? 1 : $offset));

        switch ($extension) {
            case "doc":
            case "docx":
                $adapter_class = WordAdapter::class;
                break;

            case "blade.php":
                $adapter_class = BladeAdapter::class;
                break;
        }

        return new $adapter_class($this->tpl);
    }

    protected function resolveDestination($format = "pdf", $filename = null)
    {
        // Resolve pathinfo
        if ($filename === null) {
            $pathinfo = [
                "filename" => $this->tpl_pathinfo["filename"],
                "extension" => $format,
            ];
        }
        else $pathinfo = pathinfo($filename);

        $pathinfo["dirname"] = $this->tpl_pathinfo["dirname"];

        // Build destination path
        $destination = $pathinfo["dirname"]."/".$pathinfo["filename"];

        if (!empty($pathinfo["extension"])) {
            $destination .= ".".$pathinfo["extension"];
        }

        return $destination;
    }

    protected function generateTmpFile()
    {
        return dirname($this->tpl)."/".uniqid("tmp", true)."-".basename($this->tpl);
    }

    public function to($format, $filename = null, $override = false)
    {
        // Render template
        $tmp_file = $this->generateTmpFile();
        $this->adapter->render($tmp_file);

        // Set document destination
        $destination = $this->resolveDestination($format, $filename);

        // Convert document file
        if (!file_exists($tmp_file) || $override) {
            self::convertFile($tmp_file, $format, $destination, true);
        }

        return $destination;
    }

    public static function convertFile($file, $format = "pdf", $destination = null, $unlink = false)
    {
        $file = pretty_url($file);

        if ($destination !== null) {
            $destination = '-o "'.$destination.'"';
        }

        exec("export HOME=/tmp");
        exec(self::unoconv()." -f $format $destination \"$file\" 2>&1", $output, $return);

        if (!$return && $unlink) {
            @unlink($file);
        }
    }
}