<?php

namespace Laraden\Support\Data;

use Laraden\Support\Traits\GetterSetter;

abstract class JsonRepositories 
{
    use GetterSetter;

    public $key;

    public $currentKey;

    public $primaryKey;

    protected $fillable = [];

    protected $except = ["_token", "_method"];

    public function __construct(array $attributes = [])
    {
        $this->boot();
        $this->fill($attributes);

        $this->currentKey = $this->key;
    }

    public function boot() {}

    public function getAttributes() 
    {
        return $this->attributes;
    }

    public function fill($data) 
    {
        if (is_array($data) && count($data) > 0) {
            if (is_array($this->fillable) && count($this->fillable) > 0) {
                $data = array_only($data, $this->fillable);
            }

            if (is_array($this->except) && count($this->except) > 0) {
                $data = array_except($data, $this->except);
            }

            foreach ($data as $key => $value) {
                $this->set($key, $value);
            }
        }

        return $this;
    }

    public function getByKey($key = null, $default = null)
    {
        if ($key !== null) {
            $key = "$this->currentKey.$key";
        }
        else $key = $this->currentKey;

        return file_config($key, $default);
    }

    public static function all()
    {
        $model = new static();

        return collect($model->getByKey(null, []));/*->transform(function($item) {
            return new static($item);
        });*/
    }

    public static function find($key)
    {
        $model = new static();
        $model->currentKey = "$model->key.$key";

        $data = $model->getByKey(null, []);

        if (is_array($data)) {
            return $model->fill($data);
        }
        else {
            return $data;
        }
    }

    public static function findOrFail($key)
    {
        $data = static::find($key);

        if ($data === null) {
            abort(404);
        }

        return $data;
    }

    public static function create(array $data)
    {
        $model = new static($data);

        if ($model->primaryKey !== null) {
            $model->currentKey .= $model->key.".".$data[$model->primaryKey];
        }

        $model->save();

        return $model;
    }

    public function update(array $data)
    {
        $this->fill($data)->save();
    }

    public function save()
    {
        $set = [];

        foreach ($this->attributes as $field => $value) {
            $set[$field] = $value;
        }

        $key = $this->key;

        if (!$this->exists()) {
            file_config([$key => []]);
        }

        if ($this->primaryKey !== null) {
            $key .= ".".$set[$this->primaryKey];
        }

        file_config([$key => $set]);
    }

    public function exists()
    {
        return app("storage.config")->fileExists(str_replace(".", "/", $this->key));
    }
}
