<?php

namespace Laraden\Support\Data;

class JsonStorage 
{
    private $dir_path;

    private $storage = [];

    public function __construct($dir_path)
    {
        $this->dir_path = $dir_path;
    }

    protected function resolveFilename($path)
    {
        return $this->dir_path."/".$path.".json";
    }

    public function getFromFile($file)
    {
        if (!$this->fileExists($file)) {
            return;
        }

        if (!isset($this->storage[$file])) {
            $data = json_decode(file_get_contents($this->resolveFilename($file)), true);
            $this->storage[$file] = $data;
        }

        return $this->storage[$file];
    }

    public function fileExists($file)
    {
        return file_exists($this->resolveFilename($file));
    }

    protected function saveIntoFile($path, $data)
    {
        $filepath = $this->resolveFilename($path);
        
        make_dir(dirname($filepath));
        file_put_contents($filepath, json_encode($data));
    }

    public function put($path, $data)
    {
        $file = $this->dir_path."/".str_replace(".", "/", $path);
        $this->saveIntoFile($file, $data);
    }

    public function set($keys, $value = null)
    {
        $key_list = is_array($keys) ? $keys : [$keys => $value];
        $is_file_loaded = false;

        foreach ($key_list as $keys => $value) {
            $path = "";

            foreach (explode(".", $keys) as $key) {
                // Get data from file
                if (!$is_file_loaded) {
                    $path .= "/$key";

                    if ($this->getFromFile($path) !== null) {
                        $set = &$this->storage[$path];
                        $is_file_loaded = true;
                    }

                    continue;
                }

                // create new if not exists
                if (!isset($set[$key])) {
                    if (!is_array($set)) {
                        $set = [$key => null];
                    }
                    else $set[$key] = null;
                }

                // Get reference of array index location
                $set = &$set[$key];
            }

            $set = $value;
            
            // Update config if exists
            if ($is_file_loaded) {
                $this->saveIntoFile($path, $this->storage[$path]);
            }
            // Or just create a new one
            else {
                $this->saveIntoFile($path, $value);
            }
            
            $is_file_loaded = false;
        }
    }

    public function get($key_path)
    {
        $path = "";
        $data = null;

        foreach (explode(".", $key_path) as $key) {
            $path .= "/$key";

            // Locate file
            if ($data === null) {
                $data = $this->getFromFile($path);
                continue;
            }
            
            // Locate Array Index
            if (isset($data[$key])) {
                $data = $data[$key];
            }
            else return null; // abort mission
        }

        return $data;
    }
}