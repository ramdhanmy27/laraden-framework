<?php

namespace Laraden\Support\Auth;

use App\Http\Controllers\Controller;
use Cache;
use DB;
use Storage;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission 
{
	/**
	 * register permission(s) to database
	 * 
	 * @param  array $permission 
	 * e.g ["perm-name" => "description"]
	 * @return int "affected rows"
	 */
	public function register(array $permissions) 
	{
		$data = [];
		$time = DB::raw("NOW()");

		foreach ($permissions as $item) {
			$data[] = [
				"name" => $item['name'],
				"description" => isset($item['description']) ? $item['description'] : null,
				"display_name" => title_case(substr($item['name'], strrpos($item['name'], ".")+1)),
				"created_at" => $time,
				"updated_at" => $time,
			];
		}

		return DB::table("permissions")->insert($data);
	}

	/**
	 * update permissions descripton
	 * 
	 * @param  array $permission e.g [["permName", "description"]]
	 * @return int "affected rows"
	 */
	public function updateAttributes(array $permission) 
	{
		$values = [];

		foreach ($permission as $val) {
			$values[] = "('".implode("','", $val)."')";
		}

		DB::update(
			"UPDATE permissions p set description=t.description 
			from (values ".implode(",", $values).") as t (name, description)
			where t.name::varchar=p.name"
		);
	}

	/**
	 * remove permissions from database
	 * 
	 * @param  string|array $permission e.g ["permName1", "permName2"]
	 * @return int "affected rows"
	 */
	public function deleteByName($permission) 
	{
		if (!is_array($permission)) {
			$permission = [$permission];
		}

		return DB::table("permissions")
			->whereIn("name", $permission)
			->delete();
	}

	public function initFeature(array $permissions, $prefix = "") 
	{
		$do_update = false;
		$key = "$prefix|cp";

		// get current permissions from cache 
		// or from database (if cache is not available)
		$current_data = Cache::has($key) ? Cache::get($key) : $this->getFromDatabase($prefix);
		$new_data = $this->buildData($permissions, $prefix);

		// Diff Data Permissions
		$update_diff = [];

		// insert new permissions
		$insert_diff = array_diff_ukey(
			$new_data, 
			$current_data, 
			function($x, $y) use ($current_data, $new_data, &$update_diff) {
				if ($x == $y) {
					// update description
					if ($current_data[$x]['description'] != $new_data[$x]['description']) {
						$update_diff[] = $new_data[$x];
					}

					return 0;
				}
				else return $x > $y ? 1 : -1;
			}
		);

		// update description
		if (count($update_diff) > 0) {
			$this->updateAttributes($update_diff);
			$do_update = true;
		}

		// create new permissions
		if (count($insert_diff) > 0) {
			$this->register($insert_diff);
			$do_update = true;
		}

		// remove old permissions
		$delete_diff = count($current_data)>0 ? array_diff_key($current_data, $new_data) : [];

		if (count($delete_diff) > 0) {
			$this->deleteByName(array_keys($delete_diff));
			$do_update = true;
		}

		// update cache
		if ($do_update === true) {
			Cache::forever($key, $this->getFromDatabase($prefix));
		}
	}

	public function reload()
	{
		// Init Modules
		// if modular was enable
		if (config("laraden.modular.enable")) {
			$modules = app("modular")->getRegisteredModule();

			foreach ($modules as $module_id => $module) {
				$permission_file = $module->path."/Config/permission.php";

				if (file_exists($permission_file)) {
					$this->initFeature(require $permission_file, $module_id);
				}
			}
		}

		// Init Permission
		$permission_file = base_path("config/permission.php");
		
		if (file_exists($permission_file)) {
			$this->initFeature(require $permission_file);
		}
	}

	/**
	 * parsing data permissions from controller
	 * 
	 * @param  array  $permissions 
	 * @param  string $controller  "controller name id"
	 * @return array
	 */
	public function buildData(array $permissions, $prefix = null) 
	{
		$data = [];

		foreach ($permissions as $feature => $credentials) {
			foreach ($credentials as $name => $description) {
				// Use description as name
				if (is_numeric($name)) {
					$name = $description;
					$description = null;
				}

				// Extends name with prefix
				$name = $this->resolveName($feature.".".$name, $prefix);

				$data[$name] = compact("name", "description");
			}
		}

		return $data;
	}

	public function resolveName($name, $prefix = null)
	{
		// resolve permission name manually
		if (strpos($name, ":") !== false) {
			return $name;
		}

		// use current access module id as prefix
		if (defined("MODULE_ID") && $prefix === null) {
			$prefix = MODULE_ID;
		}

		return "$prefix:$name";
	}

	/**
	 * get all permissions on specific controller
	 * 
	 * @param  string $controller "controller name id, will use current controller if its empty"
	 * @return array
	 */
	public function getFromDatabase($prefix) 
	{
		return $this->where(DB::raw("lower(name)"), "like", strtolower($prefix).":%")
			->select("name", "description")
			->get()
			->keyBy("name")
			->toArray();
	}

	public function exists($permission)
	{
		return DB::table("permissions")->where("name", $permission)->count() > 0;
	}












	/*Controllers*/

	/**
	 * clear permissions cache and re-register all controller permissions
	 * 
	 * @param  array  $dir ["namespace\path" => "@folder/path"]
	 */
	/*public function initAllController(array $dir) 
	{
		foreach ($dir as $namespace => $path) {
			$files = rglob($path."/*Controller.php");

			foreach ($files as $file) {
				$strstart = strpos($file, $path)+strlen($path)+1;
				$classname = str_replace("/", "\\", substr($file, $strstart, strrpos($file, ".php")-$strstart));
				$class = $namespace."\\".$classname;

				if (class_exists($class) && isset($class::$permission)) {
					Cache::forget("$class|cp");
					$this->initController($class, $class::$permission);
				}
			}
		}
	}*/

	/**
	 * checking for permissions update and registering permissions
	 * 
	 * @param  string  $id
	 * @param  array  $permissions
	 */
	/*public function initController($id, array $permissions) 
	{
		if (!is_array($permissions)) {
			return;
		}

		$do_update = false;
		$key = "$id|cp";

		// get current permissions from cache | database (if cache is not available)
		$current_data = Cache::has($key) ? Cache::get($key) : $this->getByController($id);
		$new_data = $this->buildData($permissions, $id);

		$update_diff = [];

		// insert new permissions
		$insert_diff = array_diff_ukey($new_data, $current_data, function($x, $y) use ($current_data, $new_data, &$update_diff) {
			if ($x == $y) {
				// update description
				if ($current_data[$x]['description'] != $new_data[$x]['description']) {
					$update_diff[] = $new_data[$x];
				}

				return 0;
			}
			else return $x > $y ? 1 : -1;
		});

		// update description
		if (count($update_diff) > 0) {
			$this->update($update_diff);
			$do_update = true;
		}

		// create new permissions
		if (count($insert_diff) > 0) {
			$this->create($insert_diff);
			$do_update = true;
		}

		// remove old permissions
		$delete_diff = count($current_data)>0 ? array_diff_key($current_data, $new_data) : [];

		if (count($delete_diff) > 0) {
			$this->delete(array_keys($delete_diff));
			$do_update = true;
		}

		// update cache
		if ($do_update === true) {
			Cache::forever($key, $this->getByController($id));
		}
	}*/

	/**
	 * get formatted permission name i.e. controller-id:permission
	 * 
	 * @param  string $permission
	 * @param  string $controller 	"controller name id"
	 * @return string
	 */
	/*public function getPermissionName($controller, $permission) 
	{
		if (strpos($permission, ":") !== false)
			return $permission;
		
		return $controller.':'.$permission;
	}*/

	/**
	 * Get permission name from current accessed controller
	 * 
	 * @param String $permission
	 */
	/*public function controller($permission) 
	{
		return $this->getPermissionName(config("controller.class"), $permission);
	}*/

	/**
	 * get all permissions on specific controller
	 * 
	 * @param  string $controller "controller name id, will use current controller if its empty"
	 * @return array
	 */
	/*public function getByController($controller) 
	{
		return $this->whereRaw("name ~* E'^".str_replace("\\", "\\\\\\\\", $controller).":.+'")
			->select("name", "description")
			->get()->keyBy("name")->toArray();
	}*/

	/*public function exists($permission)
	{
		return DB::table("permissions")->where("name", $permission)->count() > 0;
	}*/
}