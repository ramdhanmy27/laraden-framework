<?php

namespace Laraden\Services\Helpers;

use DB;
use Laraden\Support\Helpers\ExportHelper;

class BackupHelper 
{
    public $destination = "storage/app/seeds";

    public function __construct($destination)
    {
        $this->destination = base_path($destination);
    }

    public function fromTable($table_name)
    {
        return new ExportHelper(DB::table($table_name)->get(), [
            "base_path" => $this->destination,
        ]);
    }

    public function fromModel($class)
    {
        return new ExportHelper($class::all(), [
            "base_path" => $this->destination,
        ]);
    }
}