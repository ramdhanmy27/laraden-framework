<?php

namespace Laraden\Support\Helpers;

use Laraden\Support\Traits\GetterSetter;

class ExportHelper 
{
    use GetterSetter;

    const OVERRIDE = "override";

    const APPEND = "append";
    
    const RESTRICT = "restrict";

    private $csv_delimiter = ",";

    public function __construct($data, array $params = [])
    {
        $this->data = $data;
        $this->set($params);
    }

    /**
     * Export data into CSV
     * @param  string  $filepath
     * @param  boolean $include_column [need:column attribute|associated array/object]
     * @param  string  $behaviour      override behaviour
     * @throws Exception
     */
    public function toCSV($filepath, $include_column = true, $behaviour = self::OVERRIDE)
    {
        $filepath = $this->base_path.$filepath;

        // abort process
        if ($behaviour == self::RESTRICT && file_exists($filepath))
            return;

        $fopen = fopen($filepath, $behaviour == self::APPEND ? "a" : "w");

        try {
            foreach ($this->data as $i => $row) {
                $row = method_exists($row, "toArray") ? $row->toArray() : (array) $row;

                // collecting column
                if ($include_column && !isset($this->column)) {
                    if (is_assoc($row)) {
                        $this->column = array_keys($row);

                        // write column name
                        fputcsv($fopen, $this->column, $this->csv_delimiter);
                    }

                    // collecting done
                    $include_column = false;
                }

                // write data
                foreach ($row as $i => $val) {
                    switch (gettype($val)) {
                        case 'boolean':
                            $row[$i] = intval($val);
                            break;

                        case 'NULL':
                            $row[$i] = "NULL";
                            break;

                        case 'array':
                        case 'object':
                            $row[$i] = json_encode($val);
                            break;
                    }
                }

                fputcsv($fopen, $row, $this->csv_delimiter);
            }
        }
        catch(\Exception $e) {
            throw $e;
            // \Flash::error($e->getMessage());
        }

        fclose($fopen);
    }

    protected function getBasePathAttribute()
    {
        $path = $this->attributes["base_path"];

        if (isset($this->attributes["base_path"]))
            $path .= "/";

        return $path;
    }
}