<?php

namespace Laraden\Support\Menu;

use Closure;
use Event;
use Laraden\Support\Menu\Factory\Collection\CollectionProduct;
use Laraden\Support\Menu\Factory\Database\DatabaseProduct;

class MenuFactory
{
	/**
	 * Driver that will be used by default
	 * @var String
	 */
	private $default_driver;

	/**
	 * Contain all instance Drivers
	 * @var array
	 */
	private $drivers = [];

	public function __construct() 
	{
		$this->setDefaultDriver(config("menu.default", "collection"));
	}

	/**
	 * Set Default Menu Driver
	 * @param string $driver
	 */
	public function setDefaultDriver($driver) 
	{
		$this->default_driver = $driver;
	}

	/**
	 * Retrieving Driver instance
	 * or create a new one of not exists
	 * 
	 * @param  string $driver
	 * @return MenuFactoryContract
	 */
	public function driver($driver) 
	{
		if (!isset($this->drivers[$driver])) {
			$this->drivers[$driver] = $this->makeDriver($driver);
		}

		return $this->drivers[$driver];
	}

	/**
	 * Create Driver instance
	 *
	 * @throws  Exception  driver not found
	 * @param  string $driver
	 * @return MenuFactoryContract
	 */
	protected function makeDriver($driver) 
	{
		switch ($driver) {
			case "db":
				return new DatabaseProduct;
			
			case "collection":
				return new CollectionProduct;

			default:
				throw new \Exception("Driver '$driver' was not available");
		}
	}

	/**
	 * directing method to driver object
	 * @param  string $method
	 * @param  array $param
	 * @return mixed
	 */
	public function __call($method, $param) 
	{
		return call_user_func_array([$this->driver($this->default_driver), $method], $param);
	}
}
