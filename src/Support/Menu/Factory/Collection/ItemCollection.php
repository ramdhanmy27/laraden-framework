<?php

namespace Laraden\Support\Menu\Factory\Collection;

use Closure;
use Laraden\Support\Menu\Factory\Collection\Item;
use Laraden\Support\Menu\Factory\MenuContract;
use Laraden\Support\Menu\Path;
use Request;

class ItemCollection implements MenuContract
{
	public $parent;

	protected $current;

	protected $group_stack = [];

	protected $collection = [];

	protected $menu_view = "layout.menu";

	protected $tree_view = "layout.menu-tree";

	public $increment = 1;

	public function add($label, $url = null, $icon = null)
	{
		$item = new Item($this, $label, $url);

		// icon
		if ($icon !== null) {
			$item->attr(["icon" => $icon]);
		}

		// Menu Item
		if (!empty($this->parent)) {
			$item->parent = $this->parent;
			$this->parent->child[$this->increment] = $item;
		}
		else $this->collection[$this->increment] = $item;

		// Menu Group
		if ($this->hasGroup()) {
			$group = end($this->group_stack);

			if (isset($group["prefix"])) {
				if ($item->url !== null) {
					$item->url = pretty_url($group["prefix"]."/".$item->url);
				}

				unset($group["prefix"]);
			}

			if (count($group) > 0) {
				$item->attr($group);
			}
		}

		$this->increment++;

		return $item;
	}

	public function hasGroup()
	{
		return count($this->group_stack) > 0;
	}

	protected function mergePushGroup(array $attr)
	{
		$prefix_exists = isset($attr["prefix"]);

		foreach ($this->group_stack as $item) {
			// concatenating prefix url if any
			if ($prefix_exists && isset($item["prefix"])) {
				$attr["prefix"] = $item["prefix"]."/".$attr["prefix"];
			}
		}

		if ($prefix_exists) {
			$attr["prefix"] = pretty_url($attr["prefix"]);
		}

		$stack = end($this->group_stack);

		$this->group_stack[] = is_array($stack) ? array_merge($stack, $attr) : $attr;
	}

	public function group($attr, Closure $callback)
	{
		$this->mergePushGroup($attr);

		$callback($this);

		array_pop($this->group_stack);
	}

	public function checkCurrent($item)
	{
		$current_url = Request::path();
		$url_match = strpos($current_url, $item->url) !== false;

		// Skip checking on irrelevant menu items
		if ((defined("MODULE_ID") && (
			!$item->hasAttr("module") 
			|| $item->getAttr("module") != MODULE_ID
		)) || empty($item->url)) {
			return;
		}

		// Set current menu
        if ($url_match && (
        	// doesn't have current menu
        	!$this->hasCurrent() 
        	// shorter url menu
        	|| (strlen($item->url) < strlen($this->current->url))
        )) {
            $this->current = $item;
        }
	}

    public function hasCurrent() 
    {
        return !empty($this->current);
    }

	public function getCurrent() 
	{
		return $this->current;
	}

	public function getTree() 
	{
		return $this->collection;
	}

	public function setTreeView($view)
	{
		$this->tree_view = $view;

		return $this;
	}

	public function getTreeView()
	{
		return $this->tree_view;
	}

	protected function renderTree($collection) 
	{
		$html = "";
		$view = $this->getTreeView();

		// Manual Rendering
		foreach ($collection as $id => $menu) {
			$this->checkCurrent($menu);

			// credentials
			if ($menu->hasAttr("credential")) {
				$credential = $menu->getAttr("credential");

				if (is_callable($credential) && !$credential()) {
					continue;
				}
				else if (!can($credential)) {
					continue;
				}
			}

			if (is_callable($view)) {
				$html .= $view($menu, [
					"active" => $this->current==$menu,
					"dropdown" => $menu->hasChildren() ? $this->renderTree($menu->child) : null,
				]);
			}
			else {
				$html .= view($view, [
					"active" => $this->current==$menu,
					"label" => $menu->label,
					"url" => $menu->url,
					"icon" => $menu->getAttr("icon"),
					"attr" => $menu->getAttr(),
					"dropdown" => $menu->hasChildren() ? $this->renderTree($menu->child) : null,
				])->render();
			}
		}

		return $html;
	}

	public function filter($condition, Closure $callback)
	{
		if (is_callable($condition)) {
			$condition = $condition();
		}

	    $this->group(["show" => $condition], $callback);
	}

	public function setMenuView($view)
	{
		$this->menu_view = $view;

		return $this;
	}

	public function getMenuView()
	{
		return $this->menu_view;
	}

	public function render() 
	{
		$view = $this->getMenuView();
		$tree = [];

		// Filter Menu
		foreach ($this->getTree() as $id => $item) {
			if ($item->hasAttr("show") && !$item->getAttr("show")) {
				continue;
			}

			// separate by module
			if (config("laraden.modular.separation.menu")) {
				// accessing module
				if (defined("MODULE_ID")) {
					// module mismatch
					if (MODULE_ID != $item->getAttr("module")) {
						continue;
					}
				}
				// accessing global route
				else if ($item->hasAttr("module")) {
					continue;
				}
			}

			$tree[$id] = $item;
		}

		// Render Menu
		if (is_callable($view)) {
			return $view($this->renderTree($tree));
		}
		else {
			return view($view, [
				"menu" => $this->renderTree($tree),
			]);
		}
	}

	public function getPath($item = null) 
	{
		$path = new Path();

		if (empty($item) && $this->hasCurrent())
			$item = $this->getCurrent();

		if (!empty($item)) {
			$recursive = function($path, $item) use (&$recursive) {
				if (!empty($item->parent)) {
					$recursive($path, $item->parent);
				}

				$path->add($item->label, $item->url, ["icon" => $item->getAttr("icon")]);
			};

			$recursive($path, $item);
		}

		return $path;
	}

	/**
	 * Render Menu path as breadcrumb
	 * 
	 * @return View
	 */
	public function renderBreadcrumb() 
    {
		return view("layout.breadcrumb", ["path" => $this->getPath()->get()])->render();
	}
}