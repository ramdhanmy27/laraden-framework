<?php

namespace Laraden\Support\Menu\Factory\Collection;

use Closure;
use Event;
use Html;
use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Collection;
use Laraden\Support\Menu\Factory\Collection\ItemCollection;
use Laraden\Support\Menu\Menu;
use Laraden\Support\Menu\MenuItem;
use Request;

class CollectionProduct 
{
	protected $default_name = "main";

	protected $collections = [];

	public function __construct()
	{
		$this->collections[$this->default_name] = new ItemCollection();
	}

	public function make($name, Closure $callback)
	{
		if (!isset($this->collections[$name])) {
			$this->collections[$name] = new ItemCollection();
		}

		$collection = $this->collections[$name];
		$module = app("modular")->module;

		if ($module !== null) {
		    $collection->group(
		    	[
			    	"module" => $module->id, 
			    	"prefix" => config("laraden.modular.map.$module->name", $module->id),
			    ], 
				function($collection) use ($callback) {
					$callback($collection);
				}
			);
		}
		else {
			$callback($collection);
		}

		return $collection;
	}

	public function get($name)
	{
		return isset($this->collections[$name]) ? $this->collections[$name] : null;
	}

	public function __call($method, $param) 
	{
		return call_user_func_array([$this->get($this->default_name), $method], $param);
	}
}