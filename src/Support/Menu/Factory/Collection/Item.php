<?php

namespace Laraden\Support\Menu\Factory\Collection;

use Closure;
use Laraden\Support\Menu\Factory\Collection\ItemCollection;

class Item 
{
	public $container;

	public $parent;

	public $id;

	public $label;

	public $url;

	public $child = [];

	public $attributes = ["level" => 1];

	public function __construct($container, $label, $url = null) 
	{
		$this->container = $container;
		$this->id = $container->increment;
		$this->label = $label;
		$this->url = $url===null ? null : pretty_url($url);
	}

	public function attr(array $attributes)
	{
		$this->attributes = array_merge($this->attributes, $attributes);

		return $this;
	}

	public function getAttr($name = null)
	{
		if (isset($this->attributes[$name])) {
			return $this->attributes[$name];
		}
		else if ($name === null) {
			return $this->attributes;
		}
		else return null;
	}

	public function hasAttr($name)
	{
		return isset($this->attributes[$name]);
	}

	public function child(Closure $callback)
	{
		$this->container->parent = $this;

		$callback($this->container);

		$this->container->parent = null;
	}

	public function hasChildren()
	{
		return count($this->child) > 0;
	}
}
