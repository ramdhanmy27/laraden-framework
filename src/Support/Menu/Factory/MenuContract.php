<?php

namespace Laraden\Support\Menu\Factory;

interface MenuContract 
{
	public function getTree();

	public function getCurrent();

	public function getPath($id = null);

	public function render();
}
