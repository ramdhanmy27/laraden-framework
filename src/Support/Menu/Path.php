<?php

namespace Laraden\Support\Menu;

use StdClass;

class Path
{
	protected $path = [];

    public function add($label, $url = null, $attr = [])
    {
        $path = new StdClass();
        $path->label = $label;
        $path->url = $url;
        $path->attr = $attr;

        $this->path[] = $path;
    }

    public function get()
    {
        return $this->path;
    }

    public function render()
    {
        return view("layout.breadcrumb", [
            "path" => array_reverse($this->get()),
        ])->render();
    }
}
