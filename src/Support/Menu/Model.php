<?php

namespace Laraden\Support\Menu;

class Model extends \Laraden\DB\Model 
{
	protected $table = "menu";
	
    protected $fillable = [
        "label", 
        "url", 
        "parent", 
        "enable", 
        "order",
        "module",
        "param",
    ];

	protected $attributes = [
        "order" => 0,
        "enable" => false,
    ];

	public $timestamps = false;

    public function rules() 
    {
        return [
            "label" => "required|max:100",
        ];
    }

    public function setUrlAttribute($value)
    {
        $value = trim($value);
        $this->attributes["url"] = $value=="" ? null : $value;
    }

    public function setParentAttribute($value)
    {
        $this->attributes["parent"] = is_numeric($value) ? $value : null;
    }

    public function setParamAttribute($value)
    {
        $this->attributes["param"] = json_encode($value);
    }

    public function getParamAttribute($value)
    {
        return isset($this->attributes["param"]) ? json_decode($this->attributes["param"]) : null;
    }
    
    public function attributeLabels() 
    {
        return [
            "label" => trans("admin.menu.column.label"), 
            "url" => trans("admin.menu.column.url"), 
            "parent" => trans("admin.menu.column.parent"), 
            "enable" => trans("admin.menu.column.enable"), 
            "param[icon]" => trans("admin.menu.column.icon"), 
            "param[permission]" => trans("admin.menu.column.permission"), 
        ];        
    }
}
