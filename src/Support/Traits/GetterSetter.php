<?php

namespace Laraden\Support\Traits;

trait GetterSetter 
{
    protected $attributes;

    /**
     * Set Attribute value
     * @param array|string $params
     * @param mixed $value
     */
    public function set($params, $value = null)
    {
        $is_multiple_set = is_array($params);
        $params = $is_multiple_set ? $params : [$params => $value];

        foreach ($params as $param => $value) {
            // disallowed multiple set to unlisted keys on fillable
            if ($is_multiple_set 
                && isset($this->fillable) 
                && !in_array($param, $this->fillable)) {
                continue;
            }

            $method = "set".studly_case($param)."Attribute";

            method_exists($this, $method) 
                // use method setter
                ? call_user_func_array([$this, $method], [$value]) 
                // or directly set attribute
                : $this->attributes[$param] = $value;
        }
    }

    /**
     * Get Attribute Value
     * @param  string $param
     * @param  mixed $default will return this value 
     *                        if attribute is null or undefined
     * @return mixed
     */
    public function get($param = null, $default = null)
    {
        // return all attributes
        if ($param === null) {
            $attr = [];

            foreach ($this->attributes as $key => $val) {
                if ($key != null)
                    $attr[$key] = $this->get($key);
            }

            return $attr;
        }
        // return single attribute
        else {
            $method = "get".studly_case($param)."Attribute";

            return method_exists($this, $method) 
                ? call_user_func([$this, $method]) 
                : (
                    isset($this->attributes[$param])
                        ? $this->attributes[$param] 
                        : $default
                );
        }
    }

    public function __set($param, $value)
    {
        $this->set($param, $value);
    }

    public function __get($param)
    {
        return $this->get($param);
    }

    public function __isset($param)
    {
        return isset($this->attributes[$param]);
    }
}