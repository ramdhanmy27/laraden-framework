<?php

namespace Laraden\Support\Traits;

trait LaradenController
{
	/**
     * initialize meta data controller 
     * 
     * @param  string  $method  action method
     * @param  array   $param 
     * @return mixed
     */
    public function callAction($method, $param) 
    {
        // build app meta data
        // Generate controller name ID
        $class = get_called_class();

        /** store value into config */
        config([
            // Controller ID
            "controller.id" => hyphen_case(
            	preg_replace("/([\w\\\\]+?Controllers\\\\|Controller$)/", "", $class)
            ),

            // Action Controller Class
            "controller.class" => $class,

            // Action method
            "controller.method" => $method,
 
            // Action ID
            "controller.action" => hyphen_case(preg_replace("/^get/", "", $method)),
        ]);

        return call_user_func_array([$this, $method], $param);
    }
}