<?php

namespace Laraden\Support\Traits;

trait SingletonClass
{
	protected static $instance;

	public static function instance()
	{
		if (static::$instance === null) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function __callStatic($method, $args)
	{
		return call_user_func_array([static::instance(), $method], $args);
	}
}