<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Prevent Migrations
        if (!config("laraden.menu.migrate")) {
            return false;
        }

        Schema::create('menu', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->nullable();
            $table->string('label', 100);
            $table->text('url')->nullable();
            $table->string('module', 100)->nullable();
            $table->boolean('enable');
            $table->integer("order");
            $table->text("param")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Prevent Migrations
        if (!config("laraden.menu.migrate")) {
            return false;
        }
        
        Schema::dropIfExists('menu');
    }
}
