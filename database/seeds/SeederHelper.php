<?php

namespace Laraden\Seeds;

use DB;

class SeederHelper
{
    public $csv_path = "storage/app/seeds";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function seedFromCSV($file, $table, $delimiter = ",", $parse_callback = null, $field = [])
    {
        $file = fopen(base_path("$this->csv_path/".$file), "r");
        $loop = false;
        $data = [];
        $x = 0;

        // clear all data
        $this->clear($table);
        $query = is_string($table) ? DB::table($table) : $table;

        while (!feof($file)) {
            $csv = fgetcsv($file, 0, $delimiter);

            if (count($csv) == 0 || $csv == false) {
                continue;
            }

            if (!$loop) {
                // check column available
                extract($this->parseTable($table));
                $connection = $connection ? $connection : "public";
                $columns = [];

                if (is_string($table)) {
                    $columns = DB::table("information_schema.columns")
                        ->select("column_name", "udt_name", "column_default", "is_nullable", "character_maximum_length")
                        ->where("table_schema", $connection)
                        ->where("table_name", $table)
                        ->get();

                    $columns = collect($columns)->pluck("udt_name", "column_name");
                }

                // validate column
                $column = [];

                foreach ($csv as $column_name) {
                    // set column to false if not exists
                    $column[] = count($columns)==0 || isset($columns[$column_name]) ? $column_name : false;
                }

                $loop = true;
            }
            else {
                for ($i = 0; $i < count($csv); $i++) {
                    // skip/allow insertion
                    if (isset($field["except"]) && in_array($column[$i], $field["except"])
                        || isset($field["only"]) && !in_array($column[$i], $field["only"]))
                        continue;

                    // invalid column
                    if (!$column[$i])
                        continue;

                    $data[$x][$column[$i]] = is_callable($parse_callback) 
                        ? $parse_callback($csv[$i], $i, $column, $csv)
                        : $this->parseCSVData($csv[$i]);
                }

                $x++;
            } 

            // insert data secara parsial
            if (count($data) > 1000) {
                $query->insert($data);
                $data = [];
            }
        }

        // last insert
        if (count($data) > 0)
            $query->insert($data);

        fclose($file);
    }

    public function parseTable($table)
    {
        if (strpos($table, ".") !== false)
            list($connection, $table) = explode(".", $table);
        else
            $connection = null;

        return [
            "connection" => $connection,
            "table" => $table,
        ];
    }

    public function parseCSVData($value)
    {
        return $value === "NULL" ? NULL : $value;
    }

    public function clear($table)
    {
        $query = is_string($table) ? DB::table($table) : $table->query();
        $query->delete();
    }
}
