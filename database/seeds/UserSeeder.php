<?php

namespace Laraden\Seeds;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Laraden\Modules\Admin\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();

        // clear all data
        DB::table("users")->delete();

        /** Users */
        $users = [[
            "name" => "Admin",
            "email" => "admin@app.com",
            "password" => bcrypt("admin"),
            "role" => "admin",
        ]];

        for ($i = 1; $i <= 5; $i++)
            $users[] = [
                "name" => str_random(4),
                "email" => str_random(4)."@gmail.com",
                "password" => bcrypt("admin"),
                "role" => "staff",
            ];

        foreach ($users as $data) {
            $user = User::create($data);

            // attach role into user
            if (isset($roles[$data["role"]]))
                $user->attachRole($roles[$data["role"]]);
        }

        Model::unguard();
    }
}
