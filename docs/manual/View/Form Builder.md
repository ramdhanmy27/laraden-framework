#Form Builder
Validasi pada form input menggunakan form builder memiliki konsep yang sama dengan validasi client yang sudah dijelaskan pada bagian DOM event. Kita perlu menambahkan attribute `rules` pada form input disertai dengan rules nya. 

> https://laravelcollective.com/docs/5.4/html

##Form Input
###Text

```php
{!! Form::text('name', 'Maxlength', ["attr" => "value"]) !!}
```

###TextArea

```php
{!! Form::textarea('name', 'Maxlength', ["attr" => "value"]) !!}
```

###Img

```php
{!! Form::img('name', 'path/to/image.jpg', ["attr" => "value"]) !!}
```

###Select

```php
{!! Form::select(
  'name', // name
  [ // options
    "v1" => "Option 1",
    "v2" => "Option 2",
  ],
  "v1", // selected
  ["attr" => "value"], // input attributes
  ["attr" => "value"], // option attributes
) !!}
```

###Date

```php
{!! Form::date('name', date("Y-m-d")) !!}
```

###Time

```php
{!! Form::time('name', 'Time', ["attr" => "value"]) !!}
```

###Date Range

```php
$to = $from = date("Y-m-d");
{!! Form::daterange('name', [$from, $to]) !!}
```

###Date Time

```php
{!! Form::datetime('name', date("Y-m-d")) !!}
```

###Checkbox
####Single

```php
{!! Form::checkbox(
  'name', // name
  'v1', // value
  true, // checked
  ["class" => "colored-success"] // attributes
) !!}
```

####Multiple

```php
{!! Form::checkboxes(
  'name[]', // input name
  ["v1" => "label 1", "v2" => "label 2"], // checkbox options
  ["v1"] // selected options
) !!}
```

###Radio
####Single

```php
{!! Form::radio(
  'name', // name
  'v1', // value
  true, // checked
  ["class" => "colored-success"] // attributes
) !!}
```

####Multiple

```php
{!! Form::radios(
  'name[]', // input name
  ["v1" => "label 1", "v2" => "label 2"], // checkbox options
  ["v1"] // selected options
) !!}
```

###Toggle Switch

```php
{!! Form::toggleSwitch(
  'name', // name
  'v1', // value
  true, // checked
  ["class" => "colored-success"] // attributes
) !!}
```

##Form Group
Gunakan `Form Group` jika anda ingin membuat form input beserta dengan labelnya. Seperti HTML di bawah ini.

```html
 <div class="form-group">
   <label for="name" class="col-md-3 control-label">Label</label>
   <div class="col-md-6">
     <input class="form-control" name="name" value="value" id="name" type="text">
   </div>
 </div>
```

```php
{!! Form::group("input-type", "name", ["attribute" => "value"]) !!}
```

###Form Type Attributes
`label` [string]: Form Label
`value` [mixed]: Form Value
`attr` [array]: Form Html Attributes

####label
####text
####textarea
####img
####date
####time
####daterange
####dateTime
####toggleSwitch
`checked`: boolean
####select
`options`: array
####checkbox
`checked`: boolean
####checkboxes
`options`: array
####radio
`checked`: boolean
####radios
`options`: array

###Contoh Penggunaan

```php
{!! Form::group("checkboxes", "hobby", [
  "label" => "Hobbies"
  "options" => [
    "a" => "Berenang", 
    "b" => "Sepak Bola", 
    "c" => "Berkuda",
  ]
  "value" => ["a", "c"]
  "attr" => [
    "class" => "checkboxes",
    "rules" => "required",
  ],
]) !!}
```

##Validation
###Rules
Bisa juga ditambahkan manual seperti berikut.

```php
{!! Form::open() !!}
  {!! Form::rules(["number" => "required|integer|max:30"]) !!}
  {!! Form::group("text", "number") !!}
{!! Form::close() !!}
```

##Model
Keuntungan dalam menggunakan model adalah `validasi`, `label` dan `value` akan otomatis dimuat. 

```php
{!! Form::model($model) !!}
  // form input..
{!! Form::close() !!}
```

```php
use Laraden\DB\Model;

class MyModel extends Model
{ 
    // ...

    public function rules() 
    {
        return [
            "name" => "required|max:50",
            "balance" => "required|numeric|min:0",
        ];
    }

    public function attributeLabels()
    {
        return [
            "id" => "ID",
            "name" => "Model Name",
            "balance" => "Balance",
        ];
    }
}
```
