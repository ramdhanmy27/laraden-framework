#Permissions
##Introduction
###Registrasi
Untuk meregistrasikan permission, anda bisa melakukannya dengan membuat file `config/permission.php` seperti dibawah ini.

```php
return [
    // Basic 
    "feature-1" => ["add", "edit", "delete", "view"],

    // Set description on credentials
    "feature-2" => [
        "print" => "Print document as PDF",
        "filter" => "Filter document by name",
    ],
];
```

###Penggunaan

```php
/** Basic */
can("feature-1.add");

/** Multiple check */
// feature-2.print or feature-2.filter
can(["feature-1.add", "feature-1.edit"]); 
can(["feature-2.print", "feature-2.filter"], false); 

// feature-2.print and feature-2.filter
can(["feature-2.print", "feature-2.filter"], true); 
```

##Modular
Jika anda membuat module dengan nama `ModuleTest`, lokasi file untuk konfigurasi permission akan berada di `app/Modules/ModuleTest/Config/permission.php`. 

###Penggunaan
Anda tidak perlu khawatir jika terdapat permission dengan nama yang sama pada module lainnya. Karena setiap permission akan diberi awalan sesuai dengan nama modulenya. Berikut contoh untuk penggunaanya.

```php
can("module-test:feature-1.add");
```

Penulisan awalan module id pada permission bersifat optional. Anda bisa mengabaikannya jika fungsi dipanggil dalam scope module itu sendiri. Sehingga penulisannya akan menjadi seperti berikut.

```php
can("feature-1.add");
```

Anda mungkin khawatir jika pengecekan permission akan bertabrakan dengan permission non-modular yang ada pada `config/permission.php`, karena penulisannya menjadi serupa. Untuk hal tersebut, jika pengecekan ada pada scope modulenya, yang digunakan adalah permission pada `ModuleTest`. Lalu jika anda ingin mengecek permission non-modular pada scope modulenya anda bisa menuliskannya seperti ini.

```php
can(":feature-1.add");
```

##Middleware

Route::group(["middleware" => "authorize:module1:feature.show,modul2:feature.show"], function() {
	// routes..
});
