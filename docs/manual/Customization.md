#Customization
##Vendor Publish
###Modules
Secara default, module pada package Laraden sudah di load. 
Anda bisa melihatnya di index `module.path` pada file `config/laraden.php` 
terdapat `"vendor/laraden/laraden/modules" => "Laraden\\Modules\\"`. 
Namun apabila ingin mengcustomnya, mungkin anda perlu menduplikasinya dengan perintah berikut. 

Module yang terpublish akan berada di folder `app/Modules`

```
php artisan vendor:publish --tag=modules
```

###Dokumentasi
Anda bisa mempublish file dokumentasi dengan perintah berikut.

```
php artisan vendor:publish --tag=docs
```

###Theme (View & Assets)
`.env`

```
MIX_THEME=beyond
```

```
php artisan vendor:publish --tag=theme
```

##Exception Handler
```php
use Laraden\Exceptions\ServiceException;
use Laraden\Exceptions\ValidatorException;

protected $dontReport = [
    // ....

    ServiceException::class,
];

public function render($request, Exception $exception)
{
    switch (get_class($exception)) {
        case ServiceException::class:
            return $exception->response();

        case ValidatorException::class:
            \Flash::danger($exception->getMessages()->all());
            return back()->withInput($request->input());
    }

    return parent::render($request, $exception);
}
```
