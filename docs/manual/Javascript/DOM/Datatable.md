#Datatable
##Basic
Untuk membuat datatable, anda cukup menambahkan saja attribute `datatable` pada element `table`. Berikut contohnya.

```html
 <table 
    class="table table-bordered" 
    datatable 
    dt-search="false"
    dt-paginate="false" 
    dt-length="true">
     <thead>
         <tr>
             <th>ID</th>
             <th>Amount</th>
             <th>Created At</th>
             <th>Updated At</th>
         </tr>
     </thead>
     <tbody>
        <tr>
             <td>Value of ID</td>
             <td>Value of Amount</td>
             <td>Value of Created At</td>
             <td>Value of Updated At</td>
        </tr>
     </tbody>
 </table>
```

Pada contoh diatas anda dapat melihat attribut `dt-paginate="false"`. Attribut tersebut akan mendisable fitur pagination pada datatable. Jika anda ingin mengaktifkannya, atur nilainya menjadi `true` atau hapus attribut tersebut. Karena fitur pagination akan aktif secara default. Adapun beberapa attribute lainnya seperti `dt-search` untuk mengatur fitur pencarian dan `dt-length` untuk mengatur combobox pengaturan jumlah data.

##AJAX
###Server Side
Pada sisi server, gunakan facade `Datatables` untuk membuat response AJAX. Anda tidak perlu mengkhawatirkan tentang searching, sorting maupun paging pada data. 

>Untuk lebih detail penggunaan datatable, anda bisa kunjungi halaman berikut. https://github.com/yajra/laravel-datatables/wiki

```php
Route::any("contoh-transaction/data/{status}", "ContohTransactionController@data");
```

```php
use Datatables;

public function data($status) 
{
    $query = DB::table("transaction")->where("status", $status);

    return Datatables::of($query)->make(true);
}
```

Adapun jika anda ingin mengcustom data sendiri, bisa seperti dibawah ini.

```php
use Datatables;

public function data($status) 
{
    $query = DB::table("transaction")->where("status", $status);

    // Make datatables response
    $res = Datatables::of($query)->make(true);

    // Custom content response
    return response_map($res, function($data) {
        $data->action = "custom action button";
        $data->tgl_transaksi = date("d F Y", strtotime($data->tgl_transaksi));

        return $data;
    });
}
```

###Client Side
Setelah anda membuat halaman untuk merespons request datatable, tambahkan url yang mengarah pada halaman tersebut pada attribute `datatable`. Lalu berikan pula attribute `dt-field` dengan nilainya adalah nama index pada data yang anda berikan pada response. Biasanya adalah nama kolom pada table. Anda pun bisa mendisable fitur sorting atau pencarian jika perlu. Cukup tambahkan attribute `sort` atau `search` dengan nilai `false`. Adapun jika ingin menampilkan data yang bisa dicustom sendiri, tambahkan kolom table dengan attribute `dt-action` dan valuenya bernilaikan selector yang mengarah pada tag `dt-template`.

```html
 <table class="table table-bordered" datatable="{{ url("contoh-transaction/data/draft") }}">
    <thead>
        <tr>
            <th dt-action=".value-id">ID</th>
            <th dt-field="amount">Amount</th>
            <th dt-field="tgl_transaksi">Transaction Date</th>
            <th dt-action="#action-data" sort="false" search="false">Actions</th>
        </tr>
    </thead>
    <dt-template>
        <div class="value-id">
            <b>[[ id ]]</b>
        </div>
        <div id="action-data">
            <button class="btn btn-default">[[ action ]]</button>
            <a href="{{ url("contoh-transaction/[[ encodeURIComponent(id) ]]") }}" tooltip="View">
                <i class="fa fa-eye"></i>
            </a>
        </div>
    </dt-template>
 </table>
```
