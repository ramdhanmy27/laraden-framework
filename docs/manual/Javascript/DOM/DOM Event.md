#DOM Event
Akan sangat merepotkan jika kita harus menuliskan event javascript pada setiap halaman. Dan akan sangat tidak efisien apabila event tersebut sudah pernah kita buat sebelumnya. Oleh karena itu, framework ini menyediakan event-event javascript untuk anda pakai. Anda tidak perlu lagi menuliskan javascript pada setiap halaman yang anda temui. Cukup tambahkan selector dan attribut yang dibutuhkan saja.

##Tooltip
>http://getbootstrap.com/javascript/#tooltips

```html
<a href='url' tooltip='View' data-placement="left">View</a>
```

##Anchor Method Spoofing

```html
<a href='url' method="POST">Link</a>
<a href='url' method="PATCH">Link</a>
<a href='url' method="PUT">Link</a>
<a href='url' method="DELETE">Link</a>
<a href='url' method="OPTION">Link</a>
```

##Check - Uncheck All

```html
<input type="checkbox" check-all="#scope input[type='checkbox']">

<div id="scope">
    <li><input type="checkbox" /> Label 1</li>
    <li><input type="checkbox" /> Label 2</li>
    <li><input type="checkbox" /> Label 3</li>
</div>
```