#Modal
##Membuat modal
Untuk membuat modal javascript anda perlu menambahkaan htmlnya sendiri ke dalam view. Bentuk html itu sendiri sama seperti html untuk membuat modal dengan menggunakan bootstrap. Karena memang bootstrap yang digunakan :3.

Jika menuliskan htmlnya terlalu panjang bagi anda atau anda lupa susunan html-nya, anda bisa tuliskan seperti ini.

```php
include("ui.modal", [
    "id" => "my-modal-selector",
    "class" => "modal-sm",
    "attr" => ["style" => "font-weight: bold"],
    "title" => "Judul Modal",
    "body" => "<div>Content.</div>",
    "footer" => 
        '<button type="button" data-dismiss="modal" class="btn btn-default">Tidak</button>
        <button type="button" class="btn btn-primary modal-accept">Ya</button>',
])
```

Jika anda tidak memerlukan salah satu dari index diatas, anda tidak harus mengisinya dengan `null` atau string kosong `""` cukup hiraukan saja. Karena tidak ada satupun yang bersifat required.

##Pengunaan
###Basic
Untuk menggunakan modal yang sudah anda buat, anda cukup menambahkan attribut `modal` dan tentukan selector element modal yang akan dieksekusi. Berikut contoh penggunaannya.

```html
<button modal='#my-modal-selector'>Show Modal</button>
```

Ketika anda meng-klik tombol tersebut, modal dengan id `my-selector-modal` akan ditampilkan. Apabila anda tidak mengatur value pada attribut modal, secara default valuenya adalah `#modal-basic`.

###Title Modal
Title pada modal akan otomatis menggunakan title halaman yang sedang anda akses. Jika anda ingin menggunakan title yang lain, gunakan attribut `modal-title`.

```html
<button modal='#my-modal-selector' modal-title='Judul Modal'>Show Modal</button>
```

###Body Modal
Body pada modal bisa anda atur dengan attribut `modal-body`.

```html
<button modal='#my-modal-selector' modal-body='Badan Modal'>Show Modal</button>
```

###Ukuran Modal
Modal bootstrap menyediakan tiga ukuran pada modal, yaitu `sm`, `md`, dan `lg`. Untuk menentukan ukuran modal yang ingin anda tampilkan anda bisa gunakan seperti ini :

```html
<button modal-sm='#my-modal-selector'>Show Modal</button>
```

###Modal AJAX
Jika anda menambahkan attribut modal pada element `a` yang memiliki attribute `href`, content modal yang ditampilkan pada saat anda meng-kliknya adalah content halaman url pada attribute href tersebut. 

```html
<a href="url-content" modal="#modal-basic">Show Modal</button>
<!-- atau kosongkan nilai dari attribute 'modal'. Karena secara default akan mengarah ke #modal-basic -->
```
Pada saat modal melakukan request ajax pada url tersebut, aplikasi akan merespon dengan kode HTML yang mendeskripsikan halaman secara keseluruhan. Sayangnya hal tersebut tidak dibutuhkan. Kita hanya butuh kontennya saja, tanpa header, sidebar, ataupun footer. Untuk menyiasati hal tersebut, anda bisa atur pada source view yang diakses pada halaman tersebut seperti ini.


```html
extends('app')

section('content')
    <div class="panel">
        <div class="panel-heading">
            <h2 class="panel-title">Contoh Transaction</h2>
        </div>

        <div class="panel-body">
        @section("content-ajax")
            {!! Form::model(@$model) !!}
                {!! Form::group('text', 'amount') !!}
                {!! Form::group('select', 'provinsi_id')) !!}
                {!! Form::group('select', 'kota_id') !!}
                {!! Form::group('textarea', 'description') !!}

                <div class="col-md-offset-3">
                    {!! Form::submit("Simpan", ["name" => "save"]) !!}
                    {!! Form::submit("Kirim", ["name" => "sent", "class" => "btn btn-warning"]) !!}
                </div>
            {!! Form::close() !!}
        @show
        </div>
    </div>
endsection

```

Jika anda mendefinisikan section `content-ajax` pada view, aplikasi akan merespon dengan kode HTML yang berada pada section tersebut. Jika anda tidak mendefinisikannya, respon aplikasi akan mengambil dari section `content`. Jadi anda tidak perlu repot-repot memisah file view atau memfilternya di javascript.

>Catatan: Hanya berlaku pada request ajax.

##Predefined Modal
Pada framework ini sudah disediakan beberapa modal sebagai berikut.

###Modal Basic
Modal ini merupakan modal default yang akan digunakan jika anda tidak mengatur selector pada attribut modal. Modal ini hanya menampilkan header dan body saja, tanpa footer. Modal ini digunakan secara default digunakan untuk menampilkan pesan alert atau content yang di-load oleh AJAX.

```html
<a href="url-content" modal="#modal-basic">Show Modal</a>
```

####Modal Confirm
Jika anda membutuhkan konfirmasi action terhadap user, anda bisa menggunakan modal ini. Modal ini disediakan footer dengan dua tombol. Ya dan Tidak.

```html
<a href="delete/12" confirm="Apakah anda yakin ?">Hapus</a>
```

###Modal Error
Anda bisa menggunakan modal ini untuk menampilkan pesan error yang terjadi pada javascript. Modal ini menyediakan tombol untuk close dan mereload ulang halaman.
