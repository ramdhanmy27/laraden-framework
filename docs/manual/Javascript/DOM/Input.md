#Input
##Validation
Rule-rule yang tersedia, sama seperti rule validasi yang ada pada framework laravel. Proses validasi form akan ditrigger pada event `change` dan `submit` pada form.

```html
<input type="text" rules="required|email" />
```

##Select2
>https://select2.github.io/

```html
<select select2>
    <option value="1">Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```

###Multiple Select

```html
<select select2 multiple>
    <option value="1" selected>Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```

##Date Picker

```html
<input type="text" datepicker>
```

##Date Range
>http://bootstrap-datepicker.readthedocs.org/en/stable/

```html
<input type="text" daterangepicker>
```

##Time Picker

```html
<input type="text" timepicker>
```

##Datetime Picker

```html
<input type="text" datetimepicker>
```

##Toggle Switch

```html
<input type="checkbox" switch>
```

##Number Format

```html
<input type="text" num-format>
```

##Input File with Thumbnail

```html
<input type="file" preview={{ url("path/to/image.jpg")}}>
```

##File Upload

```html
<input type="file" fileupload>
```
