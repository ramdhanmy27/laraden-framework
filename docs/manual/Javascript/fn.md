#fn
##csrf
berisi nilai token csrf

```javascript
console.log(fn.csrf);
```

##url
Jika anda terlalu malas untuk menuliskan url yang panjang dengan http query yang banyak anda bisa menggunakan fungsi ini.

```javascript
fn.url("pegawai") // http://domain.com/pegawai
fn.url("pegawai", ["edit", 21]) // http://domain.com/pegawai/edit/21
fn.url("pegawai", {q: "budi", sort:"asc"}) // http://domain.com/pegawai?q=budi&sort=asc
```

##empty
Memeriksa apakah variable kosong atau tidak.

```javascript
fn.empty(undefined) // true
fn.empty(null) // true
fn.empty(" ") // true
fn.empty({}) // true
fn.empty([]) // true

fn.empty(0) // false
fn.empty("string") // false
fn.empty([1,2,3]) // false
fn.empty({a: 1, b: 2}) // false
```

##get
Mengambil data dari object dengan key location.

```javascript
var data = {a: {b: 200, c: [1, 2, 3]}};

fn.get(data, "a.b") // 200
fn.get(data, "a.c") // [1, 2, 3]
fn.get(data, "a.c.1") // 2
```

##isset
Object yang memiliki banyak level anak kadang merepotkan untuk diperiksa. Dengan fungsi ini anda dapat melakukannya seperti berikut :

```javascript
var obj = {
    a : {
        aa: 1, 
        ab: { 
            aba: 1 
        }
    }
}

fn.isset(obj.a.aa) // true
fn.isset(obj.a, "ab", "aba") // true
fn.isset(0) // true
fn.isset("") // true
fn.isset([]) // true
fn.isset({}) // true

fn.isset(obj.a.a, "abc") // false
fn.isset(obj, "a", "ab", "abc") // false
fn.isset(obj2) // false
fn.isset(undefined) // false
fn.isset(null) // false
```

##notif
Menampilkan Pesan Notifikasi

```javascript
// success | danger | info | warning/notice
fn.notif("Notification Messages", "info");
fn.notif(["Notification #1", "Notification #2"], "warning");

// dark | primary
fn.notif({
    title: "Nightmare :3",
    msg: "Dark Messages",
    icon: "fa fa-user",
    class: "notification-dark",
});

fn.notif([
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
]);
```

##format
###num
Memformat angka menjadi format uang (seperti: 10.000).

```javascript
fn.format.num("10000") // 10.000
fn.format.num("10000", 2) // 10.000,00
fn.format.num("10000", 2, ",", ".") // 10,000.00
```