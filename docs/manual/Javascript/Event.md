#Event
Dalam object ini berisi semua kebutuhan untuk mengurusi keperluan event javascript, khususnya JQuery. Berikut beberapa fungsi yang ada pada object ini.

##push
Untuk menambahkan event gunakan fungsi `push` disertai dengan callbacknya.

```javascript
fn.event.push("ui-element", function() {
    $("#element").click(function(){
        alert('wow');
    })
});
```

##trigger
Gunakan fungsi ini untuk men-trigger event yang telah dipush.

```javascript
var scopeEl = document; // default
fn.event.trigger("ui-element", scopeEl);

// atau gunakan tanda '*' untuk men-trigger semua event.
fn.event.trigger("*", scopeEl);
```

##pull
Anda pun bisa juga mengambil fungsi yang telah dipush.

```javascript
var callback = fn.event.pull("ui-element"); // callback function
```

##drop
Atau menghapusnya.

```javascript
fn.event.drop("ui-element");
```

##exists
Dan memeriksa ketersediaan event tersebut seperti ini.

```javascript
fn.event.exists("ui-element"); // false
```
