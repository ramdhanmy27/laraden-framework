#String Helpers
###hyphen_case

```php
echo hyphen_case("CamelCase"); // camel-case
echo hyphen_case("object_id"); // object-id
```

###title_case

```php
echo title_case("object-id"); // Object Id
echo title_case("object_id"); // Object Id
echo title_case("CamelCase"); // Object Id
```

##currency

```php
currency(10000); // 10.000
currency(10000, 2, "$"); // $ 10.000,00
```

##rm_slash

```php
rm_slash("///path/to/something\\\\"); // path/to/something
```

##pretty_url

```php
pretty_url("///path/////to/\\\\something\\\\"); // path/to/something
```

##is_email

```php
is_email("admin@app.com"); // true
is_email("not an email"); // false
```

##str_fill

```php
str_fill("x", 5); // xxxxx
```

##spell_amount

```php
spell_amount(10000); // sepuluh ribu
spell_amount(220); // dua ratus dua puluh
```

##code_increment

```php
code_increment("PX"); // PX-0001
code_increment("PX", "PX-0001") // PX-0002
code_increment("PX", "PX-0001", 2) // PX-02
code_increment("PX", "PX-01", 4) // PX-0002
code_increment("PX", "BJ-0001", 4, false) // PX-0002
code_increment("PX", "BJ-0001", 4, true) // PX-0001
code_increment("PX", "BJ-0001", 4, true, "/") // PX/0001
```
