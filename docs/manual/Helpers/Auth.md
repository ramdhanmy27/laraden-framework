#Auth Helpers
##can

```php
can("feature.permission");
can(["feature.permission", "feature.permission2"], true); // and operator
can(["feature.permission", "feature.permission2"], false); // or operator
```

##can_or_fail
Jika user tidak memiliki akses untuk permission, aplikasi akan me-redirect ke halaman error dengan http code 403.

```php
can_or_fail("feature.permission");
can_or_fail(["feature.permission", "feature.permission2"], true); // and operator
can_or_fail(["feature.permission", "feature.permission2"], false); // or operator
```
