#Misc Helpers
##pg_array
Convert postgre array to php array

```php
pg_array({1, 2, "input values"}); // [1, 2, "input values"]
```

##cdn
Save cdn file and load it offline

```php
// USE_OFFLINE_CDN=true
cdn("https://cdnjs.org/path/to/file-v1.js"); // public/cdn/file-v1.js

// USE_OFFLINE_CDN=false
cdn("https://cdnjs.org/path/to/file-v1.js"); // https://cdnjs.org/path/to/file-v1.js
```
