#Data Helpers
##xml_encode
Encode array into xml string.

```php
echo htmlentities(xml_encode(["data" => ["item" => [1, 2, 3]]]));

/* Hasil :
<data> 
  <item>1</item> 
  <item>2</item> 
  <item>3</item> 
</data>
*/
```

##input_filter
Filter empty string from array

```php
input_filter([
    true, 1, 0, "value", false, [], new StdClass,
    "", " ", // eliminate only empty string
]); // [true, 1, 0, "value", false, " ", [], StdClass Object]
```

##is_assoc

```php
is_assoc([2, 1]); // false
is_assoc([1 => 2, 1]); // true
is_assoc(['a' => 1, 'b' => '2']); // true
```
