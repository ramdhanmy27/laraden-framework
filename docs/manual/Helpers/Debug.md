#Debug Helpers
##dd

```php
dd([1, 2, 3]);
```

##dds
Dump data without exit

```php
dds([1, 2, 3]);
```

##trace
Debug backtrace function & methods

```php
trace();
trace([1, 2, 3]); // dump value
```

##traces
Debug backtrace function & methods without exit

```php
traces();
traces([1, 2, 3]); // dump value
```
