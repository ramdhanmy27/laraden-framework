#Http Helpers
##download
Make raw download response

```php
$target = "path/to/dir"; // path to file/dir
$download_name = "dir.zip";  // Download Name on download response
$delete_target = false; // delete file after download

download($target, $download_name, $delete_target); // download target as zip if file is directory
```

##response_map

```php
$res = Datatables::of(DB::table("gl_account_group"))->make(true);

return response_map($res, function($data) {
    $data->key = "data";

    return $data;
});
```
