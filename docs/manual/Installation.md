#Installation
##Quick Start
`composer.json`

```
"minimum-stability": "dev",
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:ramdhanmy27/laraden-framework"
    }
]
```

```
composer require ramdhanmy27/laraden-framework:dev-master
```

```php
'providers' => [
    //...
    Laraden\Providers\LaradenServiceProvider::class,
]
```

##Override Helpers
auto view, enchanced dd()
`bootstrap/autoload.php` include sebelum file `vendor/autoload.php`.

```php
define("VENDOR_DIR", __DIR__."/../vendor");

require VENDOR_DIR.'/ramdhanmy27/laraden-framework/helpers/override.php';
require __DIR__.'/../vendor/autoload.php';
```

##Assets
`.env`

```
MIX_THEME=beyond
```

```
npm install laravel-elixir gulp dotenv
// or
yarn add laravel-elixir gulp dotenv
```

```
gulp 
```

##Middleware
`Laraden\Http\Middleware\ViewResolver::class` : Ajax filter content
`Laraden\Http\Middleware\Logger::class` : Request Logger

`app/Http/Kernel.php`

```php
protected $middlewareGroups = [
    'web' => [
        // ....
        \Laraden\Http\Middleware\ViewResolver::class,
        \Laraden\Http\Middleware\Logger::class,
    ],
    // ....
];
```

##Model
validation, label

```php
use Laraden\DB\Model;

class MyModel extends Model
{
    //...
}
```

##Controller
auto view.

```php
namespace App\Http\Controllers;

use Laraden\Support\Trait\LaradenController;

class Controller extends BaseController
{
    use LaradenController;
}
```
