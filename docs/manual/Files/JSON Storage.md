#JSON Storage
##Get Value

```php
file_config("path.to.key");
file_config("path.to.key", "default value");
```

##Set Values

```php
$std = new StdClass();
$std->label = "Data Label";
$std->value = 1000;

file_config([
    "path.to.key0" => 10000,
    "path.to.key1" => "value 1",
    "path.to.key2" => ["object 1", "object 2"],
    "path.to.key3" => false,
    "path.to.key4" => $std,
]);
```
