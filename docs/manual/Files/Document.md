#Document
UNOCONV (Document Converter)
https://github.com/PHPOffice/PHPWord

##Installation
Install Libreoffice >= 5.3

###windows
add libreoffice program (`C:\Program Files\LibreOffice 5\program`) folder path to environmet variable

###Linux
####Tambah variable path di apache
Centos / RedHat : /etc/sysconfig/httpd
Debian / Ubuntu : /etc/apache2/envvars
export PATH="$PATH"

##Blade Template

```php
$convert = new Convert("path/to/report.blade.php");

$convert->data([
    "code" => "ab/0001",
    "total" => 100000,
]);

$file = $convert->to("pdf", "report.pdf", true); // override report.pdf if any

return response()->file($file);
```

##Word Document

```php
$convert = new Convert("path/to/report.docx");

$convert->data([
    "code" => "ab/0001",
    "total" => 100000,
]);

// or using template processor

$convert->data(function($tpl) {
    $tpl->setValue("code", "ab/0001", 1);
    $tpl->setValue("total", 100000, 1);
});

$file = $convert->to("pdf", "report.pdf");

return response()->download($file);
```
