#Modules
Aplikasi akan mengecek module-module yang terdaftar pada key config `laraden.modular.path`. Setiap module akan memiliki ID yang bergantung dengan nama foldernya. Dalam kasus ini, ID modulenya adalah `module-name`. Module ID ini akan digunakan di banyak tempat, seperti permission, view, config, url dan lang.

##Folder Structure

```javascript
app
 |-- Modules
      |-- ModuleName
           |-- Controllers // Controllers
           |-- Config // Config
           |-- Helpers // Fungsi tanpa class / namespace
           |-- Middleware // Middleware
           |-- Migrations // Migration
           |-- Models // Models
           |-- Providers // Providers
           |-- Resources 
           |--  |-- lang // Localization
           |--  |-- views // view aplikasi
           |--  |-- assets // Resource asset aplikasi (Javascript, CSS, Images, Font, dll)
           |-- / routes.php / // Route aplikasi
           |-- / gulpfile.js / // custom gulp khusus aplikasi
```

###Config
Setiap file config akan otomatis dimuat dengan ID module sebagai prefixnya.

```php
config("module-name");
```

###Helpers
Setiap file php yang dibuat pada folder `Helpers` akan otomatis dimuat saat module tersebut diakses.

###Providers
Providers yang tersimpan pada folder `Providers` harus memiliki akhiran `ServiceProvider`, contohnya seperti `ContohServiceProvider.php`. Anda tidak perlu meregistrasikan providernya pada file `config/app.php`, karena providernya akan otomatis dimuat saat bootstraping.

###Lang
File lang yang dimuat akan memiliki prefix dengan ID modulenya. Anda bisa menuliskannya seperti `trans("module-name:key.lang")`.

###Assets
Semua file assets akan otomatis di-copy-kan ke folder `public/app/module-name`. Peng-copy-an ini bisa di disable, jika nilai key config `laraden.modular.assets.autoload` diatur ke nilai `false`. Jika ingin mengaturnya dari file `.env` tuliskan `MODULAR_ASSETS_AUTOLOAD=false`.