<?php

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\Str;
use Laraden\Support\Helpers\Debug;

if (!function_exists("camel_case")) {
    function camel_case($value)
    {
        return Str::camel(str_replace("-", "_", $value));
    }
}

if (!function_exists("title_case")) {
    function title_case($value) 
    {
        $pos = strrpos($value, "\\");

        if ($pos !== false)
            $value = substr($value, $pos+1);

        return ucwords(trim(preg_replace("~[-_]~", " ", $value)));
    }
}

if (!function_exists("snake_case")) {
    function snake_case($value, $delimiter = "_")
    {
        return preg_replace(
            // "/($delimiter|)(\w)($delimiter\w{2,})?/", 
            "/($delimiter)(\w)$delimiter/", 
            "$1$2", 
            Str::snake($value, $delimiter)
        );
    }
}

if (!function_exists("dd")) {
    function dd() 
    {
        Debug::getInstance()->dumpWithPath(func_get_args(), 2);
        exit;
    }
}

if (!function_exists("asset")) {
    function asset($path, $secure = null) 
    {
        $module = app("modular")->current;

        return app("url")->asset(
            str_replace("app::", "app/".(empty($module) ? "" : $module->id."/"), $path), 
            $secure
        );
    }
}

if (!function_exists("view")) {
    function view($view = null, $data = [], $merge = []) 
    {
        $factory = app(ViewFactory::class);

        if (func_num_args() === 0 || $view === null) {
            return $factory;
        }

        // no view name provided
        if (is_array($view)) {
            $path = (!defined("APP_MATCHED") || !APP_MATCHED ? "" : "app::")
                .str_replace("\\", ".", config("controller.id"));

            return $factory->make($path.".".config("controller.action"), $view, $data);
        }
        else return $factory->make($view, $data, $merge);
    }
}