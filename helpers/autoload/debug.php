<?php

use Laraden\Support\Helpers\Debug;

if (!function_exists("dds")) {
    function dds() 
    {
        Debug::getInstance()->dumpWithPath(func_get_args(), 2);
    }
}

if (!function_exists("trace")) {
    function trace() 
    {
        Debug::getInstance()->traceWithPath(func_get_args(), 2);
        exit;
    }
}

if (!function_exists("traces")) {
    function traces() 
    {
        Debug::getInstance()->traceWithPath(func_get_args(), 2);
    }
}
