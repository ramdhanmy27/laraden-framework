<?php

/**
 * convert postgre array to php array
 * @param  string $str
 * @return array
 */
if (!function_exists("pg_array")) {
    function pg_array($str)
    {
        preg_match_all("/\"(.*)\"|[\w\s]+/", $str, $matches);

        return $matches[0];

        // harus di cek lebih lanjut data yang bisa masuk apa aja
        $data = [];

        foreach ($matches[0] as $i => $value) {
            $data[] = $value{0}=='"' ? $matches[1][$i] : $value;
        }

        return $data;
    }
}

/**
 * Save cdn file and load it offline
 * @param  string $str
 * @return array
 */
if (!function_exists("cdn")) {
    function cdn($url)
    {
        $dir = base_path("public/cdn");

        if (!file_exists($dir)) {
            make_dir($dir);
        }

        // Use CDN Locally
        if (config("laraden.use_offline_cdn")) {
            $filename = basename($url);
            $filepath = $dir."/".$filename;

            if (!file_exists($filepath)) {
                file_put_contents($filepath, file_get_contents($url));
            }

            // if download fails, use cdn instead
            return file_exists($filepath) ? asset("cdn/".$filename) : $url;
        }
        // Use CDN
        else {
            return $url;
        }
    }
}