<?php

if (!function_exists("rglob")) {
    function rglob($pattern, $flags = 0, $callback = null)
    {
        $files = glob($pattern, $flags);

        if (is_callable($callback)) {
            $callback(dirname($pattern), $files);
        }

        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }
}

if (!function_exists("make_dir")) {
    function make_dir($path, $split = "/") 
    {
        $tmp = "";

        foreach (array_filter(explode($split, $path)) as $dir) {
            $tmp .= $dir.'/';

            if (!file_exists($tmp) && is_writable(dirname($tmp))) {
                mkdir($tmp);
            }
        }

        return $tmp;
    }
}

if (!function_exists("rm_dir")) {
    function rm_dir($path)
    {
        foreach (glob($path."/*") as $file) {
            if (is_dir($file)) {
                rm_dir($file);
            }
            else @unlink($file);
        }

        @rmdir($path);
    }
}

if (!function_exists("disk_path")) {
    function disk_path($driver = null) 
    {
        if ($driver == null)
            $driver = config("filesystems.default");

        return config("filesystems.disks.$driver.root");
    }
}

/**
 * Create archive file from files & folders
 * @param  string|array $path path to compress
 * @return string zip filepath
 */
if (!function_exists("make_archive")) {
    function make_archive($target, $filename = null) 
    {
        set_time_limit(0);
        
        $dest = $filename != null 
            ? dirname($target)."/".$filename 
            : rtrim($target, "[\/\\\\]").".zip";

        // prepare archive file
        $zip = new \ZipArchive();

        if (!$zip->open($dest, \ZipArchive::CREATE | \ZipArchive::OVERWRITE))
            throw new \Exception("Can't make archive file.");

        // archiving files
        foreach (rglob("$target/*.*") as $file) {
            $zip->addFile($file, substr($file, strpos($file, $target)+strlen($target)+1));
        }

        $zip->close();

        return $dest;
    }
}
