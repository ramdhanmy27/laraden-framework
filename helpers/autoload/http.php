<?php

if (!function_exists("download")) {
    function download($target, $download_name = null, $delete_target = false) 
    {
        // make as archive
        if (is_dir($target)) {
            $target = make_archive($target);
        }

        // handle download request
        $chunk_size = 1024*1024;
        $download_name = empty($download_name) ? basename($target) : $download_name;

        header("Content-Length: ".filesize($target));
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Content-Description: File Transfer");
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: binary");
        header("Expires: 0");
        header("Cache-Control: must-revalidate");
        header("Content-Disposition: attachment; filename=$download_name");

        $fopen = fopen($target, "r");

        while (!feof($fopen)) {
            print(fread($fopen, $chunk_size));
            ob_flush();
            flush();
        }

        fclose($fopen);

        if ($delete_target)
            @unlink($target);
    }
}

if (!function_exists("response_map")) {
    function response_map($response, $callback)
    {
        $content = $response->getData();
        $content->data = array_map($callback, $content->data);

        return $response->setData($content);
    }
}
