<?php

if (!function_exists("can")) {
    function can($permission, $requireAll = false) 
    {
        if (!is_array($permission)) {
            $permission = [$permission];
        }

        foreach ($permission as $key => $name) {
            $permission[$key] = app("permission")->resolveName($name);
        }

        if ($user = Auth::user()) {
            return $user->can($permission, $requireAll);
        }
        else return false;
    }
}

if (!function_exists("can_or_fail")) {
    function can_or_fail($permission, $requireAll = false)
    {
        if (!can($permission, $requireAll)) {
            abort(403, trans("errors.http.403"));
        }
    }
}
