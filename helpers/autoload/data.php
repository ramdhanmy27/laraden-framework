<?php

/**
 * Eliminate empty string from Array
 * @param  array   $data
 * @return boolean
 */
if (!function_exists("input_filter")) {
    function input_filter(array $data)
    {
        return array_filter($data, function($item) {
            return !is_string($item) || strlen(trim($item));
        });
    }
}

/**
 * Convert array into xml
 * @param  array $data
 * @return string
 */
if (!function_exists("xml_encode")) {
    function xml_encode(array $data) 
    {
        $xml = '';

        foreach ($data as $key => $value) {
            // if array is sequential
            if (is_array($value) && array_keys($value)===range(0, count($value)-1)) {
                foreach ($value as $val)
                    $xml .= "<$key>".(is_array($val) ? xml_encode($val) : $val)."</$key>";
            }
            else $xml .= "<$key>".(is_array($value) ? xml_encode($value) : $value)."</$key>";
        }

        return $xml;
    }
}

/**
 * Check whether array is associative or not
 * @param  array   $data
 * @return boolean
 */
if (!function_exists("is_assoc")) {
    function is_assoc(array $data) 
    {
        return range(0, count($data)-1) !== array_keys($data);
    }
}
