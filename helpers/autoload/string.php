<?php

if (!function_exists("hyphen_case")) {
    function hyphen_case($value)
    {
        return snake_case($value, "-");
    }
}

/**
 * remove forward & trailing slashes
 * @param  string $url
 * @return string
 */
if (!function_exists("rm_slash")) {
    function rm_slash($url) 
    {
        if ($url === null) {
            return null;
        }

        return trim($url, "~[\/\\\\]~");
    }
}

if (!function_exists("pretty_url")) {
    function pretty_url($url)
    {
        return rm_slash(preg_replace("~[\/\\\\]+~", "/", $url));
    }
}

/**
 * Convert angka ke format currency
 * eg: Rp 10.000,00
 * @param  integer  $value     
 * @param  integer  $decimals  jumlah angka belakang koma
 * @param  string   $currency  format currency
 * @return string
 */
if (!function_exists("currency")) {
    function currency($value, $decimals=0, $currency=null) 
    {
        // Prefix
        if ($currency === null) {
            $currency = config("laraden.format.number.currency");
        }
        else $currency .= " ";

        // Decimals
        if ($decimals === 0) {
            $decimals = config("laraden.format.number.decimals", $decimals);
        }

        // Separators
        $decimals_sep = config("laraden.format.number.decimals_sep", ","); 
        $thousands_sep = config("laraden.format.number.thousands_sep", '.');

        return $currency.number_format($value, $decimals, $decimals_sep, $thousands_sep);
    }
}

if (!function_exists("spell_amount")) {
    function spell_amount($value)
    {
        $dasar = [
            1 => 'satu', 'dua', 'tiga', 'empat', 'lima', 
            'enam', 'tujuh', 'delapan', 'sembilan',
        ];
        $angka = [1000000000, 1000000, 1000, 100, 10, 1];
        $satuan = ['milyar', 'juta', 'ribu', 'ratus', 'puluh', ''];
        
        $i = 0;
        $str = '';
        $pecahan = '';
        $separator = (preg_match('/[^.,]*?([.,]{1}?)/', $value, $regs)) 
            ? $regs[1] 
            : false;

        if ($separator) {
            $p = explode($separator, sprintf("%s", $value));
            $pecahan = isset($p[1]) && $p[1] > 0 
                ? ' koma '.spell_amount($p[1]) 
                : '';

            $value = $p[0];
        }

        while ($value != 0) {
            $count = (int)($value / $angka[$i]);

            if ($count >= 10) {
                $str .= spell_amount($count)." ".$satuan[$i]." ";
            }
            else if ($count > 0 && $count < 10) {
                $str .= $dasar[$count]." ".$satuan[$i]." ";
            }

            $value -= $angka[$i] * $count;
            $i++;
        }

        $str = preg_replace("/satu puluh (\w+)/i", "\\1 belas", $str);
        $str = preg_replace("/satu (ribu|ratus|puluh|belas)/i", "se\\1", $str);

        return $str.$pecahan;
    }
}

if (!function_exists("is_email")) {
    function is_email($value)
    {
        return preg_match("/\w+\@\w+\.\w+/", $value);
    }
}

if (!function_exists("str_fill")) {
    function str_fill($str, $length)
    {
        if ($length <= 0) {
            return;
        }

        while ($length > 1) {
            $str .= $str;
            $length--;
        }

        return $str;
    }
}

if (!function_exists("trans_label")) {
    function trans_label($id)
    {
        $trans = trans($id);
        
        return is_array($trans) ? $id : $trans;
    }
}

if (!function_exists("code_increment")) {
    function code_increment(
        $prefix, 
        $prev_code = null, 
        $length = 4, 
        $prefix_increment = true, 
        $separator = "-"
    )
    {
        $prefix .= $separator;
        $prefix_len = strlen($prefix);
        $code_prefix = substr($prev_code, 0, $prefix_len);
        $increment = empty($prev_code) ? 1 : substr($prev_code, $prefix_len) + 1;

        // segment increment by prefix
        if ($prefix_increment && $code_prefix != $prefix) {
            $increment = 1;
        }

        return $prefix.str_pad($increment, $length, "0", STR_PAD_LEFT);
    }
}
