<?php

if (!function_exists("diff")) {
    function diff($date_1, $date_2, $format = "%a")
    {
        return date_diff(date_create($date_1), date_create($date_2))
            ->format($format);
    }
}
