<?php

if (!function_exists("file_config")) {
	function file_config($keys, $default = null)
	{
		$config = app("storage.config");

		// get value with default options if null
		if (is_string($keys) && $default !== null) {
			$data = $config->get($keys);

			return $data !== null ? $data : $default;
		}
		// set multiple values
		else if (is_array($keys)) {
			$config->set($keys);
		}
		// get value
		else {
			return $config->get($keys);
		}
	}
}