@extends("app")

@section("title", trans("action.create")." | Role")

@section("content")
  @include("role.form", [
    "model" => $model,
  ])
@endsection
