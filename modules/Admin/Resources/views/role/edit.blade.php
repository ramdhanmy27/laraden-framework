@extends("app")

@section("title", trans("action.update")." | Role")

@section("content")
  @include("role.form", [
    "model" => $model,
  ])
@endsection
