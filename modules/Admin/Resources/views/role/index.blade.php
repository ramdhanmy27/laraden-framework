@extends("app")

@section("title", "Role")

@section("content")
  @if (can("admin:role.add"))
    <a href="{{ url("admin/role/add") }}" class="btn btn-primary">
      <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>
  @endif

  <table class="table table-bordered" datatable="{!! url("admin/role/data") !!}">
    <thead>
      <tr>
        <th dt-field="display_name"> Name </th>
        <th dt-field="description"> Description </th>
        <th dt-col="#dt-action" sort="false" search="false"> </th>
      </tr>
    </thead>
    
    <dt-template>
      <div id="dt-action">
        @if (can("admin:role.edit"))
          <a href="{{ url("admin/role/edit/[[id]]") }}" 
            tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
            <i class="fa fa-edit"></i>
          </a>
        @endif
        @if (can("admin:role.delete"))
          <a href="{{ url("admin/role/delete/[[id]]") }}" 
            method="DELETE"
            tooltip="@lang("action.delete")" 
            class="btn btn-sm btn-danger"
            confirm="@lang("confirm.delete")">
            <i class="fa fa-trash"></i>
          </a>
        @endif
      </div>
    </dt-template>
  </table>
@endsection
