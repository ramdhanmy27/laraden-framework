@extends("app")

@push("style")
  <style>
    .fixed-button {
      position: fixed;
      top: 20%;
      right: 0;
    }
    .fixed-button .btn {
      border-radius: 5px 0px 0px 5px;
      box-shadow: -1px 1px 2px #999 !important;
    }
  </style>
@endpush

@section("content")
  {!! Form::open() !!}
    <ul class="nav nav-tabs" id="myTab">
      <li class="active">
        <a data-toggle="tab" href="#non-modular"> Credentials </a>
      </li>
      <li class="tab-red">
        <a data-toggle="tab" href="#modular"> Modules </a>
      </li>
    </ul>

    <div class="tab-content">
      <div id="non-modular" class="tab-pane in active">
        @include("user.permissions-table", [
          "roles" => $roles, 
          "features" => $non_modular,
        ])
      </div>

      <div id="modular" class="tab-pane">
        @foreach ($modular as $module => $features)
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h4 class="m-none">
                {{ config("$module.app.name", title_case($module)) }}
              </h4>
            </div>

            @include("user.permissions-table", compact("roles", "features"))
          </div>
        @endforeach
      </div>
    </div>

    <div class='fixed-button'>
      <input class="btn btn-warning" type="submit" value="Simpan" />
    </div>
  {!! Form::close() !!}
@endsection