<table class="table table-responsive table-bordered table-striped table-hover panel-body">
	<thead>
		<tr class="success">
			<th>Permission Name</th>

			@foreach ($roles as $id => $role)
				<th>{{ $role }}</th>
			@endforeach
		</tr>
	</thead>

	<?php $colspan = count($roles) + 1; ?>

	@foreach ($features as $feature => $credentials)
		<tr class="info"> 
			<td colspan="{{ $colspan }}">{{ title_case($feature) }}</td> 
		</tr>
		
		{{-- Permissions --}}
		@foreach ($credentials as $item)
			<tr>
				<td>
					<span>{{ $item->display_name }}</span>
					<h6 class="text-muted">{{ $item->description }}</h6>
				</td>

				@foreach ($roles as $id => $role)
					<td> 
						{!! Form::toggleSwitch(
							"perm[$id][]", 
							$item->id, 
							in_array($id, $item->roles), 
							["class" => "switch-sm switch-info"]
						) !!}
					</td>
				@endforeach
			</tr>
		@endforeach
	@endforeach
</table>