@extends("app")

@section("title", trans("action.create")." | User")

@section("content")
  @include("user.form", [
    "model" => $model,
  ])
@endsection
