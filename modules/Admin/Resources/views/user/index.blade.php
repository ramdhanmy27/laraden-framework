@extends("app")

@section("title", "User")

@section("content")
  @if (can("admin:user.add"))
    <a href="{{ url("admin/user/add") }}" class="btn btn-primary">
      <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>
  @endif

  <table class="table table-bordered" datatable="{!! url("admin/user/data") !!}">
    <thead>
      <tr>
        <th dt-field="name"> Name </th>
        <th dt-field="email"> Email </th>
        <th dt-field="created_at"> Register </th>
        <th dt-col="#dt-action" sort="false" search="false"> </th>
      </tr>
    </thead>
    
    <dt-template>
      <div id="dt-action">
        @if (can("admin:user.edit"))
          <a href="{{ url("admin/user/edit/[[id]]") }}" 
            tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
            <i class="fa fa-edit"></i>
          </a>
        @endif
        
        @if (can("admin:user.delete"))
          <a href="{{ url("admin/user/delete/[[id]]") }}" 
            method="DELETE"
            tooltip="@lang("action.delete")" 
            class="btn btn-sm btn-danger"
            confirm="@lang("confirm.delete")">
            <i class="fa fa-trash"></i>
          </a>
        @endif
      </div>
    </dt-template>
  </table>
@endsection
