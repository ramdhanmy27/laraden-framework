<?php 

use Laraden\Modules\Admin\Models\Role;

?>

{!! Form::model($model) !!}
  @if (!$model->exists)
    {{ Form::rules(["password_confirmation" => "required"]) }}
  @endif

  {!! Form::group("text", "name") !!}
  {!! Form::group("text", "email") !!}
  {!! Form::group("password", "password") !!}
  {!! Form::group("password", "password_confirmation", ["label" => "Confirm Password"]) !!}
  {!! Form::group("checkboxes", "roles[]", [
    "label" => "Roles", 
    "options" => Role::pluck("display_name", "id"),
    "value" => isset($roles) ? $roles : [],
  ]) !!}

  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
    </div>
  </div>
{!! Form::close() !!}