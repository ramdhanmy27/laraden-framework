@extends("app")

@section("title", "Menu")

@section("content")
  @include("menu.form", [
    "model" => $model,
  ])
@endsection
