{!! Form::model($model) !!}
  {!! Form::group('text', 'title') !!}
  {!! Form::group('text', 'url') !!}
  {!! Form::group('select', 'parent', ["options" => $options["parent"]]) !!}
  {!! Form::group('checkbox', 'enable') !!}
  {!! Form::group('select', 'param[icon]', ["options" => getIconOptions()]) !!}
  {!! Form::group('select', 'param[permission]', [
    "options" => \App\Models\Permission::get()
      ->keyBy("id")
      ->map(function($item) {
        return permission_group(substr($item->name, 0, strrpos($item->name, ":")))
          ." / $item->display_name"
          ."<br><span style='font-size: 11px'>$item->description</span>";
      })
  ]) !!}

  <div class="form-group">
    <div class="col-md-offset-3 col-md-9">
      {!! Form::submit(trans("action.save")) !!}
    </div>
  </div>
{!! Form::close() !!}