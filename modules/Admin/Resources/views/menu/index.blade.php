@extends("app")

@section("title", "Menu")

<?php

function makeTree(array $list) 
{
  if (count($list) === 0)
    return;

  $html = "";

  foreach ($list as $item) {
    $html .= "<li class='dd-item ".($item->enable ?: "inactive")."' data-id='$item->id'>"
          ."<div class='dd-handle hover-sibling'>"
            ."<a href='".(empty($item->url) ? "#" : url($item->url))."'>"
              .(isset($item->icon) ? "<i class='fa-fw $item->icon'></i>" : "")
              .trans_label($item->title)
            ."</a>"
          ."</div>"
          ."<div class='action-button show-child'>"
            .(can("admin:menu.edit")
              ? "<a href='".url("admin/menu/switch-enable/$item->id")."' method='post'>"
                  .($item->enable ?
                    "<span class='label label-success' tooltip='Click to Disable'>Enabled</span>" :
                    "<span class='label label-danger' tooltip='Click to Enable'>Disabled</span>")
                ."</a> | "
              : "")
            .(can("admin:menu.add")
              ? "<a href='".url("admin/menu/add/$item->id")."' modal>
                  <i class='fa fa-plus'></i> ".trans("action.create")
                ."</a> | "
              : "")
            .(can("admin:menu.edit")
              ? "<a href='".url("admin/menu/edit/$item->id")."' modal>
                  <i class='fa fa-pencil'></i>  ".trans("action.update")
                ."</a> | "
              : "")
            .(can("admin:menu.delete")
              ? "<a href='".url("admin/menu/delete/$item->id")."' confirm method='post' method='DELETE'>
                  <i class='fa fa-trash'></i>  ".trans("action.delete")
                ."</a> | "
              : "")
          ."</div>"
          .(isset($item->child) ? makeTree($item->child) : "")
        ."</li>";
  }

  return "<ol class='dd-list'>$html</ol>";
}

?>

@section("content")
  @if (can("admin:menu.add"))
    <a href="{{ url("admin/menu/add") }}" class="btn btn-default" modal>
      <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr />
  @endif

  <div class="dd" id="menu-tree">
    {!! makeTree(Menu::driver("db")->getTree()) !!}
  </div>
@endsection

@push("style")
  <style>
    #menu-tree .hover-parent{
      position: relative;
    }
    #menu-tree .inactive{
      opacity: 0.7
    }
    #menu-tree .action-button{
      position: absolute;
      top: 7px;
      right: 20px;
    }
  </style>
@endpush

@push("app-script")
  <script src="{{ asset("js/lib/jquery.nestable.js") }}" type="text/javascript"></script>
  <script src="{{ asset("app/admin/js/menu.js") }}" type="text/javascript"></script>
@endpush
