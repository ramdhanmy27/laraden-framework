<?php

namespace Laraden\Modules\Admin\Models\Auth;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait {
        can as private entrustCan;
    }

    public function can($permission, $requireAll = false)
    {
        if (!config("laraden.auth.permission.enable")) {
            return true;
        }

        return $this->entrustCan($permission, $requireAll);
    }
}
