<?php

namespace Laraden\Modules\Admin\Models\Auth;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $fillable = ["name", "display_name", "description"];

    protected function setNameAttribute($value)
    {
        $this->attributes["name"] = $value;
        $this->attributes["display_name"] = title_case($value);
    }

    protected function setDisplayNameAttribute($value)
    {
        $this->attributes["name"] = strtolower(str_replace(" ", "_", $value));
        $this->attributes["display_name"] = $value;
    }

    public function users()
    {
        return $this->belongsToMany(
            Config::get('auth.providers.users.model'), 
            Config::get('entrust.role_user_table'), 
            Config::get('entrust.role_foreign_key'), 
            Config::get('entrust.user_foreign_key')
        );
    }
}