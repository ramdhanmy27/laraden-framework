<?php

namespace Laraden\Modules\Admin\Models;

use Laraden\DB\Model;

class Menu extends Model 
{
    protected $fillable = ["parent", "title", "url", "enable", "order", "param"];

    public function rules() 
    {
        return [
            "title" => "required|max:100",
        ];
    } 

    protected $attributes = [
        "order" => 0,
        "enable" => false,
    ];

    public function setUrlAttribute($value)
    {
        $value = trim($value);
        $this->attributes["url"] = $value=="" ? null : $value;
    }

    public function setParentAttribute($value)
    {
        $this->attributes["parent"] = is_numeric($value) ? $value : null;
    }

    public function setParamAttribute($value)
    {
        $this->attributes["param"] = is_array($value) ? json_encode($value) : $value;
    }

    public function getParamAttribute($value)
    {
        return isset($this->attributes["param"]) ? json_decode($this->attributes["param"]) : null;
    }
    
    public function attributeLabels() 
    {
        return [
            "title" => trans("admin.menu.column.title"), 
            "url" => trans("admin.menu.column.url"), 
            "parent" => trans("admin.menu.column.parent"), 
            "enable" => trans("admin.menu.column.enable"), 
            "param[icon]" => trans("admin.menu.column.icon"), 
            "param[permission]" => trans("admin.menu.column.permission"), 
        ];        
    }
}
