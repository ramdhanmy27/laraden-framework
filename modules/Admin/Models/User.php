<?php

namespace Laraden\Modules\Admin\Models;

use DB;
use Laraden\DB\Model;
use Laraden\Exceptions\ValidatorException;

class User extends Model
{
    protected $fillable = ["name", "email", "password", "roles"];

    protected $hidden = ["password", "remember_token"];

    private $tmp = [];

    public function rules()
    {
        $rules = [
            "name" => "required|string|max:255",
            "email" => "required|email|string|max:255|unique:users",
            "password" => "string|min:6",
        ];

        // on Create
        if (!$this->exists) {
            $rules["password"] = "required|".$rules["password"];
            $rules["roles"] = "required";
        }
        // On Update
        else {
            $rules["email"] .= ",".$this->email;
        }

        return $rules;
    }

    public function registerRoles(array $roles) 
    {
        if (!$this->exists) {
            return false;
        }

        DB::transaction(function() use ($roles) {
            $data = [];
            $id = $this->getKey();

            foreach ($roles as $role_id) {
                $data[] = ["role_id" => $role_id, "user_id" => $id];
            }

            DB::table("role_user")->where("user_id", $id)->delete();
            DB::table("role_user")->insert($data);
        });
    }

    public function beforeSave()
    {
        // hash password
        if ($this->isDirty("password")) {
            $this->password = bcrypt($this->password);
        }
    }

    public function afterSave()
    {
        if (isset($this->tmp["roles"]) && is_array($this->tmp["roles"])) {
            $this->registerRoles($this->tmp["roles"]);
        }
    }

    public function setRolesAttribute($roles)
    {
        if (is_array($roles)) {
            $this->tmp["roles"] = $roles;
        }
    }
}