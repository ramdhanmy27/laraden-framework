<?php

namespace Laraden\Modules\Admin\Models;

use Laraden\DB\Model;

class Role extends Model 
{
	protected $fillable = ["name", "display_name", "description"];

    public function rules() 
    {
        return [
            "display_name" => "required",
        	"description" => "max:255",
        ];
    }

    protected function setNameAttribute($value)
    {
        $this->attributes["name"] = $value;
        $this->attributes["display_name"] = title_case($value);
    }

    protected function setDisplayNameAttribute($value)
    {
        $this->attributes["name"] = strtolower(str_replace(" ", "_", $value));
        $this->attributes["display_name"] = $value;
    }
}