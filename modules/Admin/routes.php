<?php

Route::group(["middleware" => ["web", "auth", "authorize:admin:user.view,admin:role.view,admin:menu.view"]], function() {
    Route::get("/", "UserController@index");
    Route::match(["get", "post"], "permissions", "UserController@permissions");

    Route::group(["prefix" => "user", "middleware" => "authorize:admin:user.view"], function() {
        Route::get("/", "UserController@index");
        Route::get("data", "UserController@data");
        Route::match(["get", "post"], "add", "UserController@add")->middleware("authorize:admin:user.add");
        Route::match(["get", "post"], "edit/{id}", "UserController@edit")->middleware("authorize:admin:user.edit");
        Route::delete("delete/{id}", "UserController@delete")->middleware("authorize:admin:user.delete");
    });
    
    Route::group(["prefix" => "role", "middleware" => "authorize:admin:role.view"], function() {
        Route::get("/", "RoleController@index");
        Route::get("data", "RoleController@data");
        Route::match(["get", "post"], "add", "RoleController@add")->middleware("authorize:admin:role.add");
        Route::match(["get", "post"], "edit/{id}", "RoleController@edit")->middleware("authorize:admin:role.edit");
        Route::delete("delete/{id}", "RoleController@delete")->middleware("authorize:admin:role.delete");
    });

    Route::group(["prefix" => "menu", "middleware" => "authorize:admin:menu.view"], function() {
        Route::get("/", "MenuController@index");
        Route::get("data", "MenuController@data");
        Route::match(["get", "post"], "add", "MenuController@add")->middleware("authorize:admin:menu.add");
        Route::match(["get", "post"], "edit/{id}", "MenuController@edit")->middleware("authorize:admin:menu.add");
        Route::delete("delete/{id}", "MenuController@delete")->middleware("authorize:admin:menu.add");
    });
});

Menu::make("main", function($menu) {
    $menu->add("Menu", "menu", "fa fa-bars")->attr([
        "show" => config("laraden.menu.migrate"),
        "credential" => "admin:menu.view",
    ]);
    $menu->add("Roles", "role", "fa fa-address-card")->attr([
        "credential" => "admin:role.view",
    ]);
    $menu->add("Permissions", "permissions", "fa fa-lock")->attr([
        "credential" => "admin:user.permission",
    ]);
    $menu->add("Users", "user", "fa fa-users")->attr([
        "credential" => "admin:user.view",
    ]);
});
