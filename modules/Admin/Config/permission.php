<?php

$permission = [
	"role" => [
		"view" => "Show Role",
		"add" => "Add Role",
		"edit" => "Edit Role",
		"delete" => "Delete Role",
	],
	"user" => [
		"view" => "Show Menu",
		"add" => "Add Menu",
		"edit" => "Edit Menu",
		"delete" => "Delete Menu",
		"permission" => "Config Permission",
	],
];

if (config("laraden.menu.migrate")) {
	$permission["menu"] = [
		"view" => "Show Menu",
		"add" => "Add Menu",
		"edit" => "Edit Menu",
		"delete" => "Delete Menu",
	];
}

return $permission;