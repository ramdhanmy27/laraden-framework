<?php

namespace Laraden\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laraden\Modules\Admin\Models\Role;
use Yajra\Datatables\Facades\Datatables;

class RoleController extends Controller
{
    public function index()
    {
        return view(["model" => new Role]);
    }

    public function data()
    {
        return Datatables::of(Role::select("*"))->make(true);
    }

    public function add(Request $req)
    {
        if ($req->isMethod("POST")) {
            Role::createOrFail($req->all());

            return redirect("admin/role");
        }

        return view(["model" => new Role]);
    }

    public function edit(Request $req, $id)
    {
        $model = Role::findOrFail($id);

        if ($req->isMethod("POST")) {
            $model->updateOrFail($req->all());
            
            return redirect("admin/role");
        }

        return view(["model" => $model]);
    }

    public function delete($id)
    {
        Role::findOrFail($id)->delete();
        
        return redirect("admin/role");
    }
}
