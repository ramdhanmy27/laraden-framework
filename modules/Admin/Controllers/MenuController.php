<?php

namespace Laraden\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laraden\Exceptions\ServiceException;
use Laraden\Facades\Menu;
use Laraden\Modules\Admin\Models\Menu as Model;

class MenuController extends Controller {

	public function index() 
	{
		return view([]);
	}

	public function add(Request $req, $parent = null) 
	{
		if ($req->isMethod("POST")) {
	        Model::createOrFail($req->input());

	        return redirect("admin/menu");
		}

		$model = new Menu;

		// default value
		$model->enable = true;
		$model->parent = $parent;

		return view("menu.add", [
			"model" => $model,
			"options" => [
				"parent" => $this->getMenuTreeOptions(),
			],
		]);
	}

	public function edit(Request $req, $id) 
	{
		$model = Model::findOrFail($id);

		if ($req->isMethod("POST")) {
	        $model->update($req->input());

	        return redirect("admin/menu");
	    }

		return view("menu.edit", [
			"model" => $model,
			"options" => [
				"parent" => $this->getMenuTreeOptions($id),
			],
		]);
	}

	public function delete($id) 
	{
		$model = Model::findOrFail($id);

		// move entire child to grand parent
		Model::where("parent", $model->id)->update(["parent" => $model->parent]);

		// delete menu and fix order
        $model->delete();
        Menu::driver("db")->fixOrder();

		return back();
	}

	public function switchEnable($id) 
	{
        $model = Model::findOrFail($id);
        $model->update(["enable" => !$model->enable]);

		return back();
	}

	public function order(Request $req) 
	{
		$success = Menu::driver("db")->setOrder(
			$req->input("id"), 
			$req->input("parent"), 
			$req->input("order")
		);

		if (!$success) {
			throw new ServiceException("Failed to update position");
		}

        Menu::driver("db")->fixOrder();
	}

	/**
	 * return tree menu as fetch key pair array
	 * @param  array|int $except
	 * @return array
	 */
	private function getMenuTreeOptions($except = null)
	{
		$data = [];

		foreach (Menu::getCollection() as $id => $menu) {
			// skip branches
			if (count(array_intersect(is_array($except) ? $except : func_get_args(), $menu->path)) > 0)
				continue;

			$data[$id] = str_fill(" - - ", $menu->level).trans_label($menu->title);
		}

		return $data;
	}
}
