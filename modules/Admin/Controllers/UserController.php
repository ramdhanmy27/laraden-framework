<?php

namespace Laraden\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Laraden\Exceptions\ValidatorException;
use Laraden\Modules\Admin\Models\Role;
use Laraden\Modules\Admin\Models\User;
use Yajra\Datatables\Facades\Datatables;

class UserController extends Controller 
{
    public function index() 
    {
        return view("user.index");
    }

    public function data()
    {
        return Datatables::of(User::select("*"))->make(true);
    }

    public function add(Request $req) 
    {
        if ($req->isMethod("POST")) {
            // password confirmation does not match
            if ($req->password != $req->password_confirmation) {
                throw new ValidatorException("Password confirmation does not match");
            }

            User::createOrFail($req->all());

            return redirect("admin/user");
        }

        return view(["model" => new User]);
    }

    public function edit(Request $req, $id) 
    {
        $model = User::findOrFail($id);

        if ($req->isMethod("POST")) {
            // password confirmation does not match
            if ($req->password != $req->password_confirmation) {
                throw new ValidatorException("Password confirmation does not match");
            }

            $model->updateOrFail($req->all());

            return redirect("admin/user");
        }

        return view("user.edit", [
            "model" => $model,
            "roles" => DB::table("role_user")->where("user_id", $id)->pluck("role_id")
        ]);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        
        return redirect("admin/user");
    }

    public function permissions(Request $req) 
    {
        if ($req->isMethod("POST")) {
            $data = [];

            foreach ($req->input("perm", []) as $role_id => $permissions) {
                foreach ($permissions as $perm) {
                    $data[] = [
                        "permission_id" => $perm,
                        "role_id" => $role_id,
                    ];
                }
            }

            if (count($data) > 0) {
                DB::transaction(function() use ($data) {
                    DB::table("permission_role")->delete();
                    DB::table("permission_role")->insert($data);
                });
            }

            return back();
        }

        app("permission")->reload();

        $data = DB::select(
            "SELECT p.*, pr.role_id from permissions p
            left join permission_role pr on pr.permission_id=p.id
            order by p.name, p.display_name"
        );

        $permissions = [];

        foreach ($data as $item) {
            $module_sep_pos = strpos($item->name, ":");
            $module = substr($item->name, 0, $module_sep_pos);
            $feature = substr($item->name, $module_sep_pos + 1, strpos($item->name, ".") - $module_sep_pos - 1);

            if (!isset($permissions[$module])) {
                $permissions[$module] = [];
            }

            if (!isset($permissions[$module][$feature])) {
                $permissions[$module][$feature] = [];
            }

            if (!isset($permissions[$module][$feature][$item->id])) {
                $item->roles = [];
                $permissions[$module][$feature][$item->id] = $item;
            }

            $permissions[$module][$feature][$item->id]->roles[] = $item->role_id;
        }

        // separates modular & non-modular credentials
        if (isset($permissions[""])) {
            $non_modular = $permissions[""];
            unset($permissions[""]);
        }

        return view([
            "roles" => Role::orderBy("display_name")->pluck("display_name", "id"),
            "non_modular" => isset($non_modular) ? $non_modular : [],
            "modular" => $permissions,
        ]);
    }
}