var elixir = require('laravel-elixir');
var laraden = require("./vendor/ramdhanmy27/laraden-framework/resources/laraden-elixir")(elixir)

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// require('dotenv').config()
// laraden.theme(process.env.MIX_THEME)

/** Core Assets */
elixir(function(mix) { 
	laraden.compile();
	
  mix
    .styles([
      // Laraden Vendor
      "./resources/assets/css/vendor.css",

      // Additional Vendor
      "ui/dataTables.bootstrap.css",
      "ui/typicons.min.css",

      // Theme CSS
      "beyond.min.css",
      "input.css",
      "font.css",
      // "helpers.css",
    ], "public/css/app.css")
    .copy(path +"/fonts", "public/fonts")

    .scripts([
      // Laraden Vendor
      "./resources/assets/js/vendor.js",
      
      // Theme JS
      "jquery.slimscroll.min.js",
      "beyond.min.js",
      "skins.min.js",
    ], "public/js/app.js")

	mix.scripts([
		"./public/js/app.js",
		"axios.min.js",
		"vue.min.js",
		"init.js",
	], "public/js/app.js")
});
