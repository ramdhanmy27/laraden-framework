module.exports = function(mix, path) {
  mix
    .styles([
      // Laraden Vendor
      "./resources/assets/css/vendor.css",

      // Theme CSS
      "input.css",
      "font.css",
      "beyond.min.css",

      // Additional Vendor
      "ui/dataTables.bootstrap.css",
      "ui/typicons.min.css",
    ], "public/css/app.css")
    .copy(path +"/fonts", "public/fonts")

    .scripts([
      // Laraden Vendor
      "./resources/assets/js/vendor.js",
      
      // Theme JS
      "jquery.slimscroll.min.js",
      "beyond.min.js",
      "skins.min.js",
    ], "public/js/app.js")
};

