@extends("app")

@section("content")
	<div class="panel panel-default col-md-6 col-md-offset-3">
		<div class="panel-body">
			<h3 class="page-header mt-sm mb-xlg">@yield("title")</h3>
			@yield("content-auth")
		</div>
	</div>
@endsection