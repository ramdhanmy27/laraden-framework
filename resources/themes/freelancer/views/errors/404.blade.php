@extends("app")

@section("content-inverse", true)

@section("content")
    <div class="jumbotron text-center bg-none mt-xlg">
        <h1 style="font-size: 150px">404</h1>
	    <hr class="star-light"></hr>
        <h2>Page Not Found</h2>
    </div>
@endsection
