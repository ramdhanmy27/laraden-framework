<!DOCTYPE HTML>
<html class="fixed">
    <head>
        <title>
            @hasSection("title")
                @yield("title")
            @else
                @section("title") ERP @show
            @endif
        </title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ asset("img/favicon.ico") }}">
        <link rel="stylesheet" href="{{ asset("css/app.css") }}">
        @stack("style")
    </head>
    <body id="page-top" class="index">
        <div id="skipnav"><a href="#maincontent">Skip to main content</a></div>
    
        <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" 
                        data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> 
                        Menu <i class="fa fa-bars"></i>
                    </button>
                    
                    <a class="navbar-brand" href="{{ url("/") }}">
                        <i class="fa fa-rocket"></i> ERP
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right mr-md">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="page-scroll">
                            <a href="{{ url("/") }}">
                                <i class="fa fa-home"></i> Home
                            </a>
                        </li>

                        @if (Auth::check())
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                    <img src="{{ asset("img/user-default.jpg") }}" 
                                        height="20px" class="img-circle" />

                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                    
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url("tss") }}" class="text-dark">
                                            <i class="fa fa-dashboard"></i> Dashboard
                                        </a> 
                                    </li>
                                    <li>
                                        <a href="{{ url("tss/settings") }}" class="text-dark">
                                            <i class="fa fa-cog"></i> Settings
                                        </a> 
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{ url('logout') }}" class="text-dark" method="POST">
                                            <i class="fa fa-power-off"></i> @lang("action.logout")
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li class="page-scroll">
                                <a href="{{ url("register") }}">
                                    <i class="fa fa-user"></i> Sign Up
                                </a>
                            </li>
                            <li class="page-scroll">
                                <a href="{{ url("login") }}">
                                    <i class="fa fa-lock"></i> Login
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        @yield("content-layout")

        <!-- Footer -->
        <footer class="text-center">
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; Your Website 2016
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
        <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
            <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>

        <!-- Modals -->
        @include("ui.modal", ["id" => "modal-basic"])
        @include("ui.modal", ["id" => "modal-error"])
        @include("ui.modal", [
            "id" => "modal-confirm", 
            "footer" => 
                "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>"
                    .trans("action.yes")
                ."</button>
                <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>"
                    .trans("action.no")
                ."</button>"
        ])

        <!-- Javascript -->
        <script src="{{ asset("js/app.js") }}"> </script>
        <script type="text/javascript">
            fn.url.base = '{{ url("/") }}/';
            
            $(document).ready(function() {
                @if (Notif::exists())
                    var notification = {!! json_encode(Notif::pull()) !!};

                    for (var type in notification) 
                        fn.notif(notification[type], type);
                @endif
            })
        </script>
        @stack("script")
    </body>
</html>
