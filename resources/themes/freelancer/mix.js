module.exports = function(mix) {
  mix
    .styles([
      // Laraden Vendor
      "./resources/assets/css/vendor.css",

      // Theme CSS
      "input.css",
      "theme.css",
    ], "public/css/app.css")
    .scripts([
      // Laraden Vendor
      "./resources/assets/js/vendor.js",
      
      // Theme JS
      "theme.js",
    ], "public/js/app.js")
};

