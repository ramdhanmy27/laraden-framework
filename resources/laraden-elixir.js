var path = require("path");
var fs = require('fs');

var elixir;
var base_path = "vendor/ramdhanmy27/laraden-framework";

var laraden = function(laravel_elixir) {
	elixir = laravel_elixir;

	return laraden;
} 

laraden.theme = function(theme) {
	elixir.config.sourcemaps = false;
	laraden.compile(base_path);

	var theme_dir = path.join(base_path, "resources/themes", theme);

	try {
		if (!fs.statSync("./"+ path.join(theme_dir, "mix.js")).isFile()) {
			return;
		}

		elixir(function(mix) {
			var asset_path = elixir.config.assetsPath;
			elixir.config.assetsPath = path.join(theme_dir, "assets");

			require("./"+ path.join("themes", theme, "mix.js"))(mix, elixir.config.assetsPath);
			elixir.config.assetsPath = asset_path;
		});
  }
  catch(err) {
  	console.log("The '"+ theme +"' theme could not be found.\n")
  }
};

laraden.module = function(dir) {
	elixir(function(mix) {
    // collecting module directories
    fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      })
      .map(function(module) {
      	var module_id = module.replace(/([A-Z])/g, "-$1")
      		.replace(/^\-/, "").toLowerCase()
      		.replace(/(-|)(\w)(-\w{2,})?/g, "$2$3")

			  // Make Symlink Asset
				fs.symlink(
					path.join("../../", dir, module, "Resources/assets"),
					path.join("public/app", module_id), 
					"dir",
					function(err) { /*if (err) console.log(err);*/ }
				);

				// Copy assets
        // mix.copy(
        //     path.join(dir, module, "resources/assets"), 
        //     path.join("./public/app", module_id)
        // );
      });
	});
}

laraden.compile = function(base_path) {
	laraden.module("app/Modules");

	var asset_path = elixir.config.assetsPath;
	var asset_laraden = path.join(base_path || ".", "resources/assets");

	elixir.config.assetsPath = asset_laraden;

	elixir(function(mix) { 
		mix
			// Styles
      .less("all.less", "./resources/assets/css/all.css")
			.styles([
        // Vendor
        "bootstrap.min.css",
        "font-awesome.min.css",
        "helpers.css",

        // Form Input
        "input/daterangepicker.css",
        // "input/daterangepicker.min.css",
        // "input/bootstrap-datepicker.min.css",
        // "input/bootstrap-timepicker.min.css",
        "input/select2.min.css",
        
        // UI Elements
        "ui/jquery.dataTables.min.css",
        "ui/datatables.css",
        "ui/pnotify.custom.css",
        "./resources/assets/css/all.css", // all less
      ], "./resources/assets/css/vendor.css")
      .copy(asset_laraden +"/css/lib", "public/css/lib")
      .copy(asset_laraden +"/fonts", "public/fonts")
      .copy(asset_laraden +"/img", "public/img")

			// Script
      .scripts([
        // Vendor
				"lodash.min.js",
				"jquery.min.js",
				"bootstrap.min.js",
				"lib/moment.min.js",

				// Form Input
				"input/validator.min.js",
				"input/bootstrap-datepicker.js",
				"input/bootstrap-datetimepicker.min.js",
				"input/bootstrap-timepicker.min.js",
				"input/daterangepicker.min.js",
				"input/select2.min.js",
				"input/jquery.inputmask.bundle.js",
				// "input/jquery.fileupload.min.js",
				// "input/bootstrap-maxlength.js",
				
				// UI Elements
				"ui/pnotify.custom.js",
				"ui/jquery.dataTables.min.js",

				// App JS
				"app/fn.js",
				"app/jquery/fn.jquery.js",
				"app/bootstrap/fn.bootstrap.js",

				"app/event.js",
				"app/bootstrap/event.bootstrap.js",
				"app/jquery/event.jquery.js",
				"app/jquery/event.ui.js",
				"app/jquery/event.form.js",
				"app/jquery/event.form.validation.js",
				"app/jquery/init.js",
      ], "./resources/assets/js/vendor.js")
      .copy(asset_laraden +"/js/lib", "public/js/lib")
	});

	elixir.config.assetsPath = asset_path;
};

module.exports = laraden;