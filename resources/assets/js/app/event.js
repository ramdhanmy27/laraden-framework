fn.event = {
  funcs: {__tmp: []},

  push: function(name, callback) {
    // temporary events
    if (typeof arguments[0] === "function") {
      var name = "__tmp";
      var callback = arguments[0];
    }

    if (typeof callback === "function") {
      // append callback if array
      if (_.isArray(this.funcs[name])) {
        this.funcs[name].push(callback);
      }
      else this.funcs[name] = callback;
    }
  },

  trigger: function(name, scope) {
    var scope = scope || document;

    // trigger event by name
    if (fn.event.exists(name)) {
      return this.funcs[name](scope);
    }
    // trigger all
    else if (name === "*") {
      for (var name in this.funcs) {
        // skip __tmp
        if (name !== "__tmp") {
          this.funcs[name](scope)
        }
      }
    }
    // trigger temporary
    else {
      for (var i in this.funcs.__tmp) {
        this.funcs.__tmp[i]()
      }

      // clear temporary
      this.funcs = [];
    }
  },

  exists: function(name) {
    return this.funcs.hasOwnProperty(name);
  },

  pull: function(name) {
    return this.funcs[name];
  },

  drop: function(name) {
    if (fn.event.exists(name)) {
      delete this.funcs[name];
    }
  },
}